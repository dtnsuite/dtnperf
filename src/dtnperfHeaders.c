/** \file dtnperfHeaders.c
 *
 * \brief This file contains the functions used to serialize or deserialize the dtnperf header
 *
 * \copyright Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \author Andrea Belano, andrea.belano@studio.unibo.iwaitForNextCC_Windowt
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#include <unified_api.h>

#include "dtnperf_system_libraries.h"
#include "dtnperfHeaders.h"


#include "dtnperf_definitions.h"
//#include "utils.h"

uint64_t ntoh64(uint64_t n);
uint64_t hton64(uint64_t n);

/******************************************************************************
 * \par Function Name:
 *      serializeDtnperfHeader
 *
 * \brief Function to serialize the DTNperf header structure into a buffer
 *
 * \return numBytes							The buffer length
 * \param[in]	dtnperfHeader			    The DTNperf header structure to serialize
 * \param[out]	buffer	         	        The buffer
 * \par Date Written:
 *      20/06/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------

 *  20/06/22 | C. Caini        |   Implementation
 *  17/11/22 | C. Caini        |   Lifetimes and seq. numbers fields increased from 4 to 8 B
 *****************************************************************************/


int serializeDtnperfHeader (dtnperfHeader_t dtnperfHeader, void* buffer) {
	void* cursor;
	int numBytes=MIN_DTNPERF_HEADER_SIZE; //header fixed part length
//	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Serialize dtnperf header...\n");
	uint64_t temp64;
	uint32_t temp32;
	uint8_t temp8;
	cursor = buffer;
	switch (dtnperfHeader.type) {
	case 0: //Time/Data
//First raw
		memcpy(cursor, &(dtnperfHeader), 1);		//Version & Type
		cursor += 1;

		temp8 = dtnperfHeader.ack_QoS.flag + dtnperfHeader.ack_QoS.priority * 64;
		memcpy(cursor, &temp8, 1);
		cursor += 1;

		memset(cursor, 0, 2);	//2B padding
		cursor += 2;
//Raws 2&3
		temp64 = hton64(dtnperfHeader.ack_lifetime);
		memcpy(cursor, &temp64, 8);
		cursor += 8;
//3 raws of padding
		memset(cursor, 0, 12);	//12B padding
		cursor += 12;

		break;

	case 1: //File
		numBytes += dtnperfHeader.file_name_length;	//add variable part length

//First raw
		memcpy(cursor, &(dtnperfHeader), 1);		//Version & Type
		cursor += 1;

		temp8 = dtnperfHeader.ack_QoS.flag + dtnperfHeader.ack_QoS.priority * 64;
		memcpy(cursor, &temp8, 1);
		cursor += 1;

		memcpy(cursor, &(dtnperfHeader.file_name_length), 1);
		cursor += 1;

		memset(cursor, 0, 1);	//1B padding
		cursor += 1;

//Raws 2&3
		temp64 = hton64(dtnperfHeader.ack_lifetime);
		memcpy(cursor, &temp64, 8);
		cursor += 8;

//rows 4-6
		temp32 = htonl(dtnperfHeader.file_exp);
		memcpy(cursor, &temp32, 4);
		cursor += 4;

		temp32 = htonl(dtnperfHeader.file_size);
		memcpy(cursor, &temp32, 4);
		cursor += 4;

		temp32=htonl(dtnperfHeader.segment_offset);
		memcpy(cursor, &temp32, 4);
		cursor += 4;

//End of fixed part
		memcpy(cursor, dtnperfHeader.file_name, strlen(dtnperfHeader.file_name));

		break;

	case 2: //ACK
		numBytes += dtnperfHeader.bundlex_src_eid_length;	//add variable length

//First raw
		memcpy(cursor, &(dtnperfHeader), 1);		//Version & Type
		cursor += 1;

		memset(cursor, 0, 2);	//2B of padding
		cursor += 2;

		memcpy(cursor, &(dtnperfHeader.bundlex_src_eid_length), 1);
		cursor += 1;

//Second & third  raws
		temp64 = hton64(dtnperfHeader.bundlex_timestamp.time);
		memcpy(cursor, &temp64, 8);
		cursor += 8;

//rows 4&5
		temp64=hton64(dtnperfHeader.bundlex_timestamp.seqno);
		memcpy(cursor, &temp64, 8);
		cursor += 8;

//row 6
		memset(cursor, 0, 4);	//4B of padding
		cursor += 4;

//End of fixed part
		memcpy(cursor, dtnperfHeader.bundlex_src_eid, strlen(dtnperfHeader.bundlex_src_eid));

		break;

	case 3: //Stop
//First raw
		memcpy(cursor, &(dtnperfHeader), 1);		//Version & Type
		cursor += 1;

		memset(cursor, 0, 3);	//3B of padding
		cursor += 3;

//Second and third rows
		temp64 = hton64(dtnperfHeader.lifetime);
		memcpy(cursor, &temp64, 8);
		cursor += 8;

//Fourth row
		temp32 = htonl(dtnperfHeader.sent_bundles);
		memcpy(cursor, &temp32, 4);
		cursor += 4;

//2 rows of padding
		memset(cursor, 0, 8);	//The last 8 bytes of the fixed length part are unused
		cursor += 8;

		break;

	default: //Wrong headerType
		al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Wrong dtnperf header type\n");
		return -1;
			break;
	} //Type switch
//	*headerLength=numBytes;
	return numBytes;
}


/******************************************************************************
 *
 * \par Function Name:
 *      getDtnperfHeader
 *
 * \brief Function to get the DTNperf header structure from the payload of the received bundle
 *
 *
 * \return al_error
 *
 * \param[in]	bundle					   	The al_bp bundle
 * \param[in]	header			          	The header to fill
 * \param[in]	payload_length	         	The length of the payload
 *
 * \par Date Written:
 *      20/06/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  20/06/22 | A. Belano       |   Implementation
 *****************************************************************************/

dtnperf_error_t getDtnperfHeader(al_types_bundle_object bundle,
		dtnperfHeader_t* header,
		uint32_t payload_length) {

	void* buffer=NULL;

	uint8_t location = bundle.payload->location;
	int fd;
	uint8_t opts;

	memset(header, 0, sizeof(*header));

	if (location == BP_PAYLOAD_FILE) {
		buffer = (void*)malloc(275);
		fd = open(bundle.payload->filename.filename_val, O_RDONLY);
		if (fd < 0) {
			printf("[WARNING]: could not open file %s\n", bundle.payload->filename.filename_val);
			return WARNING;
		}
		if (read(fd, buffer, 275) < 0) {
			printf("[WARNING]: could not read file %s\n", bundle.payload->filename.filename_val);
			return WARNING;
		}
		close(fd);
	} else if (location == BP_PAYLOAD_MEM) {
		buffer = bundle.payload->buf.buf_val;
	}

	memcpy(header, buffer, 1);	//Copying Version & Type (for all types)

	if (header->version != DTNPERF_HEADER_VERSION) {
//CC a more accurate control is in order should a spurious bundle arrive, either deliberately sent by an attacker or not.
		printf("[WARNING]: non DTNperf bundle received or wrong version (expected %u but received %u)\n", DTNPERF_HEADER_VERSION, header->version);
		return WARNING;
	}

	switch (header->type) {
		case 0:		//Time/Data
//Next byte of the first raw (the last 2B of padding skipped)
			opts = *((uint8_t *) (buffer + 1));
			header->ack_QoS.flag = opts & 0x01;
			header->ack_QoS.priority = opts >> 6;
//Second and third rows
			header->ack_lifetime = ntoh64(*((uint64_t *) (buffer + 4)));
			break;

		case 1:		//File
//Next 2B of the first raw (the last B of padding is skipped)
			opts = *((uint8_t *) (buffer + 1));
			header->ack_QoS.flag = opts & 0x01;
			header->ack_QoS.priority = opts >> 6;
			header->file_name_length = *((uint8_t *) (buffer + 2));

//Raws 2&3
			header->ack_lifetime = ntoh64(*((uint64_t *) (buffer + 4)));
//Raws 4-6
			header->file_exp = ntohl(*((uint32_t *) (buffer + 12)));
			header->file_size = ntohl(*((uint32_t *) (buffer + 16)));
			header->segment_offset = ntohl(*((uint32_t *) (buffer + 20)));
//End fixed part
			if (MIN_DTNPERF_HEADER_SIZE+header->file_name_length > payload_length) {
				printf("[WARNING]: file name length exceeds bundle payload length\n");
				return WARNING;
			}

			char* name = (char *) malloc(header->file_name_length + 1);
			memcpy(name, buffer + MIN_DTNPERF_HEADER_SIZE, header->file_name_length);
			name[header->file_name_length] = '\0';

			header->file_name = name;

			break;

		case 2:		//Ack
//Last B of the first raw (bytes 2 and 3 of padding are skipped)
			header->bundlex_src_eid_length = *((uint8_t *) (buffer + 3));
//Raws 2&3
			header->bundlex_timestamp.time = ntoh64(*((uint64_t *) (buffer + 4)));
//Raws 4&5
			header->bundlex_timestamp.seqno = ntoh64(*((uint64_t *) (buffer + 12)));
			if (MIN_DTNPERF_HEADER_SIZE+header->bundlex_src_eid_length > payload_length) {
				printf("[WARNING]: eid name length exceeds bundle payload length\n");
				return WARNING;
			}
//End fixed part
			char* eid = (char *) malloc(header->bundlex_src_eid_length + 1);
			memcpy(eid, buffer + MIN_DTNPERF_HEADER_SIZE, header->bundlex_src_eid_length);
			eid[header->bundlex_src_eid_length] = '\0';
			header->bundlex_src_eid = (char *) malloc(header->bundlex_src_eid_length + 1);
			strcpy(header->bundlex_src_eid, eid);
			free (eid);
			break;

		case 3:		//Stop
//Raws 2&3
			header->lifetime = ntoh64(*((uint64_t *) (buffer + 4)));
//Raw 4
			header->sent_bundles = ntohl(*((uint32_t *) (buffer + 12)));
			break;

		default:
			printf("[WARNING]: unsupported type (%u)\n", header->type);
			return WARNING;
	}

	if (location == BP_PAYLOAD_FILE) {
		free(buffer);
	}

	return SUCCESS;
}

uint64_t ntoh64(uint64_t n) {

    #if (!ARCHITECTURE_IS_BIG_ENDIAN)

    unsigned char out[8] = {n>>56, n>>48, n>>40, n>>32, n>>24, n>>16, n>>8, n};
    return *(uint64_t *) out;

    #endif

    return n;

}


uint64_t hton64(uint64_t n) {

    #if (!ARCHITECTURE_IS_BIG_ENDIAN)

    unsigned char out[8] = {n>>56, n>>48, n>>40, n>>32, n>>24, n>>16, n>>8, n};
    return *(uint64_t *) out;

    #endif

    return n;

}
