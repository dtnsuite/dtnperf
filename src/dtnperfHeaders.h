/** \file dtnperfHeaders.h
 *
 * \brief This file contains the prototypes of the functions used to serialize or deserialize the dtnperf header
 *
 * \copyright Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \author Andrea Belano, andrea.belano@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef DTNPERFHEADERS_H_
#define DTNPERFHEADERS_H_


#include "unified_api.h"
#include "dtnperf_definitions.h"

#define MIN_DTNPERF_HEADER_SIZE 24

typedef struct {
	unsigned flag:1;
	unsigned priority:2;
} QoS;


typedef struct {

	unsigned version:4;	//All headers have the Version and Type fields, 1B total
	unsigned type:4;

	union {

		struct {	//Time/Data and File
			QoS ack_QoS;
			uint8_t file_name_length;//end of first 4B row
			al_types_timeval ack_lifetime;
			uint32_t file_exp;
			uint32_t file_size;
			uint32_t segment_offset;
			char* file_name;
		};

		struct {	//Ack
			uint8_t bundlex_src_eid_length;//end of first 4B row
			al_types_creation_timestamp bundlex_timestamp;
			char* bundlex_src_eid;
		};

		struct {	//Stop
			al_types_timeval lifetime;
			uint32_t sent_bundles;
		};

	};

} dtnperfHeader_t;


int serializeDtnperfHeader (dtnperfHeader_t dtnperfHeader, void* buffer);
dtnperf_error_t getDtnperfHeader(al_types_bundle_object bundle, dtnperfHeader_t* header, uint32_t payload_length);


#endif /* DTNPERFHEADERS_H_ */
