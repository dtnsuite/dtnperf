/** \file dtnperf_definitions.h
 *
 * \brief This file contains a few definitions used in the dtnperf application
 *
 * \copyright Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \author Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 */


#ifndef DTNPERF_DEFINITIONS_H_
#define DTNPERF_DEFINITIONS_H_

#include <stdint.h>

#include "al/types/al_types.h"

// dtnperf version
#define DTNPERF_VERSION "4.1.0"

// system architecture
#ifndef ARCHITECTURE_IS_BIG_ENDIAN
#define ARCHITECTURE_IS_BIG_ENDIAN 0	//0 - Intel		1 - Internet
#endif

// the number of seconds to wait before checking again for incomplete files
#define FILE_CHECK_PERIOD 10

// the number of files to check before stopping; if 0 the thread scans all files
#define FILES_TO_CHECK_PER_CYCLE 5

// dir where are saved transfered files
#define FILE_DIR "/dtnperf_files"

// dir where are saved monitor logs
#define MONITOR_LOGS_DIR_DEFAULT "."

// default filename of the unique csv logfile for the monitor with oneCSVonly option
#define MONITOR_UNIQUE_CSV_FILENAME "monitor_unique_log.csv"

//default Monitor Session expiration
#define MONITOR_DEFAULT_SESSION_EXPIRATION 120

//dtnperf headers
#define DTNPERF_HEADER_VERSION 1

// client endpoint demux string (without PID)
#define CLIENT_DEMUX "dtnperf:/src"

// server endpoint demux string //CCaini demux without delimiter "/" as in RFC9171
#define SERVER_DEMUX "dtnperf:/dest"
#define SERVER_SERVICE_NUMBER 2000

// monitor endpoint demux string
#define MONITOR_DEMUX "dtnperf:/mon"
#define MONITOR_SERVICE_NUMBER 1000

//The separator between dtn EID node and demux
#define DTN_SCHEME_SEPARATOR "/"

// unix time of 1/1/2000
#define DTN_EPOCH 946684800

#define INTER_ACK_TIMEOUT 120
// For defining x > y
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))
// For defining x < y
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

typedef enum dtnperf_error_t
{
	SUCCESS=0,
	ERROR=1,
	WARNING=2,
} dtnperf_error_t;

typedef enum {
  IPN_scheme= 1,
  DTN_scheme = 2,
} eid_scheme_t;

#endif /* DTNPERF_DEFINITIONS_H_*/
