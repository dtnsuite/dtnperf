/********************************************************
 **  Authors: Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **           Anna d'Amico, anna.damico@studio.unibo.it
 **           Carlo Caini (DTNperf_3 project supervisor), carlo.caini@unibo.it
 **
 **
 **  Copyright (c) 2013, Alma Mater Studiorum, University of Bologna
 **  All rights reserved.
 ********************************************************/

#ifndef UTILS_H_
#define UTILS_H_


#include <sys/time.h>
#include <inttypes.h>

#include <unified_api.h>

// Automatically generated CRC function
// polynomial: 0x104C11DB7
extern uint32_t crc_table[];

uint32_t calc_crc32_d8(uint32_t crc, uint8_t *data, int len);
void sub_time(struct timeval min, struct timeval sub, struct timeval * result);
int getPayloadLength(al_types_bundle_object bundle_object);

uint32_t get_current_dtn_time();

#endif /*UTILS_H_*/
