/** \file monitorPrint.h
 *
 *  \brief  This file contains the prototypes of functions implemented in print.c.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **
 *\authors
 *  	Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *      Anna d'Amico, anna.damico@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/


#ifndef STATUS_REPORT_PRINT_H_
#define STATUS_REPORT_PRINT_H_

#include <string.h>
#include <sys/time.h>

#include <unified_api.h>


void csv_print_rx_time(FILE * file, struct timeval time, struct timeval start_time);
void csv_print_eid(FILE * file, al_types_endpoint_id eid);
void csv_print_timestamp(FILE * file, al_types_creation_timestamp timestamp);
void csv_print_status_report_timestamps_header(FILE * file);
void csv_print_status_report_timestamps(FILE * file, al_types_bundle_status_report status_report);
void csv_print_ulong(FILE * file, unsigned long num);
void csv_print(FILE * file, char * string);
void csv_end_line(FILE * file);
void csv_print_bundle_x_information(FILE * file, al_types_bundle_status_report status_report);
void printRealtimeStatusReport(FILE *f, al_types_endpoint_id  sr_source, al_types_bundle_status_report * status_report);



#endif /* STATUS_REPORT_PRINT_H_ */
