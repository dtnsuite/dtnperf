/** \file Monitor.h
 *
 *\brief  This file contains the prototype of functions and structures implemented in Monitor.c that should be seen outside the Monitor
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Benedetta Rogato
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#ifndef MONITOR_H_
#define MONITOR_H_


#include <unified_api.h>

#include "../../dtnperf_definitions.h"


typedef enum
{
	NONE,
	STATUS_REPORT,
	CLIENT_STOP,
	IRF_REPORT,
} bundle_type_t;

typedef struct session
{
	al_types_endpoint_id  client_eid;
	char * full_filename;
	FILE * file;
	struct timeval * start;
	uint64_t last_bundle_time; // time of bp creation timestamp (64 bit to allow for ms time in bpv7)
	uint64_t expiration;
	int delivered_count;
	int total_to_receive;
	struct timeval * stop_arrival_time;
	uint32_t wait_after_stop;
	struct session * next;
	struct session * prev;
}session_t;

typedef struct session_list
{
	session_t * first;
	session_t * last;
	int count;
}session_list_t;


typedef struct dtnperf_monitor_options
{
	int 							expiration_session; 				// expiration time of session log file [120]
	boolean_t						oneCSVonly;							// monitor opens a unique session and a unique csv log file [FALSE]
	boolean_t						rtPrint;							// monitor prints realtime human readable status report info [FASLE]
	FILE*							rtPrintFile;						// monitor prints realtime SR info on this file [stdout]

} monitor_options_t;


dtnperf_error_t runMonitor(dtn_suite_options_t dtn_suite_options, application_options_t unparsed_options);

#endif /* MONITOR_H_ */
