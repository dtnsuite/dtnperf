/** \file Monitor.c
 *
 *  \brief  This file contains the main Monitor functions of DTNperf v4.
 *  	The Monitor aim is to collect bundle status report information into .csv files to be later processed by a spreadsheet;
 *  	It can be used with DTNperf Client and Server or alone with other applications.
 *  	It can also be used to see bundle status report in real time.
 *
 *  \details The Monitor main function is the runMonitor; The Monitor consists of a main thread and an auxiliary thread.
 *  	The runMonitor contains the main thread; its core (an infinite while) receives bundles (usually BSR) and put the relevant information
 *  	into a .csv file (one for each Client session, unless oneCSVoption is set).
 *  	The .csv files are normally closed once the Monitor receives as many BSR "delivered" as bundle sent by the related Client session.
 *  	To let the Monitor know this number, the Client sends the Monitor a Stop bundle at the end of the session, carrying this information.
 *  	If the maximum waiting time of a session has elapsed (this information item is carried by the Stop bundle too) before receiving all
 *  	expected BSR "delivered", the file .csv must be closed automatically.
 *  	This is the task of the auxiliary thread, the "session expiration timer". It is started inside the runMonitor (unless the oneCSVoption
 *  	is set); it periodically checks if .csv files can be closed.
 *  	As for the Server, only one instance of the Monitor is allowed on the same machine. To stop the Monitor you need to press ctrl+C.
 *  	This is intercepted by the "closing mmonitor handler" (acvtive on runMonitor), which in turns calls the "monitor clean exit" function.
 *  	This sets to true  the forced_close variable, used by the auxiliary thread to terminate. As this variable is written by the main thread
 *  	it is protected by a mutex and can be written/read by a couple of set/get functions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Benedetta Rogato
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/


#include "Monitor.h"
#include "status_report_print.h"
#include "../../dtnperfHeaders.h"
//#include "../../../unified_api/src/al_bp/al_bp.h" //CCaini because there are a few al_bp aux functions

#include "../../utils.h" //CCaini Its functions should be moved somewhere else

#include "../../dtnperf_system_libraries.h"


/*
 * Global Variables
 */

// pthread variables
static pthread_t session_exp_timer;
static sem_t csv_file_sem;

static session_list_t * session_list;

static boolean_t oneCSVonly;
static al_types_bundle_object bundle_received;

static pthread_mutex_t mutex_forced_close;
static boolean_t forced_close = FALSE;



/*
 * Prototypes
 */
void dtnperf_monitor_options_set_default(monitor_options_t *monitor_options);
dtnperf_error_t monitorParsing(application_options_t unparsed_options, monitor_options_t *monitor_options);
void printMonitorHelp(boolean_t al_bundle_options_help);
dtnperf_error_t dtnperf_monitor_get_bundle_parameters(al_types_bundle_status_report ** status_report,al_types_endpoint_id * bundle_source_addr, al_types_creation_timestamp * bundle_creation_timestamp,
dtnperfHeader_t *b_header, bundle_type_t * bundle_type);
dtnperf_error_t processBundle (monitor_options_t monitor_options);

void closing_monitor_handler(int signal);
void monitor_clean_exit(int status, al_types_bundle_object bundle_received, al_socket_registration_descriptor registration_descriptor);
boolean_t get_closing_monitor();
void set_closing_monitor(boolean_t value);

void print_BSR(struct timeval current, struct timeval start,
		FILE * file, al_types_endpoint_id bundle_source_addr,
		al_types_creation_timestamp bundle_creation_timestamp, al_types_bundle_status_report status_report);

void print_bundleStop(struct timeval current, struct timeval start,
		FILE * file, al_types_endpoint_id bundle_source_addr, al_types_creation_timestamp bundle_creation_timestamp);

//Used with --oneCSVonly option
session_t * unique_session_create(char * full_filename, FILE * file, struct timeval start, uint64_t bundle_timestamp_secs);
session_t * session_create(al_types_endpoint_id  client_eid, char * full_filename, FILE * file, struct timeval start,
		uint64_t bundle_timestamp_time);
void session_destroy(session_t * session);
void session_put(session_list_t * list, session_t * session);
session_t * session_get(session_list_t * list, al_types_endpoint_id  client);
void session_del(session_list_t * list, session_t * session);
void session_close(session_list_t * list, session_t * session);
//session expiration timer thread
void * session_expiration_timer(void * opt);
void create_new_session_file(bundle_type_t bundle_type, struct timeval current,
		FILE ** file, al_types_endpoint_id relative_source_addr, al_types_creation_timestamp relative_creation_timestamp,
		al_types_creation_timestamp bundle_creation_timestamp, al_types_bundle_status_report * status_report);
void update_session(session_t * session, bundle_type_t bundle_type,
//void update_session(session_t * session, bundle_type_t bundle_type,
		FILE ** file, al_types_creation_timestamp bundle_creation_timestamp, al_types_bundle_status_report * status_report);
session_list_t * session_list_create();
void session_list_destroy(session_list_t * list);


/******************************************************************************
 *
 * \par Function Name:
 *      runMonitor
 *
 * \brief  Main function of the Monitor
 *
 * * \par Date Written:
 *  	15/02/20
 *
 * \return dtnperf_error_t					The error code
 *
 * \param[in]	unparsed_options			Options not recognized by the al_bp parser
 * \param[in]	registration_descriptor		Identifier of the registration
 *
 *\par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | -----------------| -----------------------------------------------
 *  xx/xx/13 | M. Rodolfi   	| V3 Implementation.
 *  1/06/22  | B. Rogato  		| Initial Implementation of V4
 *  11/07/22 | C. Caini			| Revised to call the new "processBundle" function
 *****************************************************************************/
dtnperf_error_t runMonitor(dtn_suite_options_t dtn_suite_options, application_options_t unparsed_options) {
	al_socket_registration_descriptor registration_descriptor;
	monitor_options_t monitor_options;
	al_error alError;
	dtnperf_error_t dtnperf_error;

	dtnperf_monitor_options_set_default(&monitor_options);
	dtnperf_error=monitorParsing(unparsed_options, &monitor_options);
    if (dtnperf_error) return ERROR; //wrong options or --help

    alError=al_socket_register(&registration_descriptor, MONITOR_DEMUX,MONITOR_SERVICE_NUMBER, dtn_suite_options.eid_format_forced, dtn_suite_options.ipn_local, dtn_suite_options.ip, dtn_suite_options.port);
    if (alError) return ERROR;

    oneCSVonly=monitor_options.oneCSVonly;

	session_list = session_list_create();

	if(oneCSVonly)
		printf("Opening monitor in Unique session mode. CTRL^C to exit\n");

	signal(SIGINT, closing_monitor_handler);//ctrl+C handler

	// create a bundle object
	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] create the received bundle structure...");
	alError = al_bundle_create(&bundle_received);
	if (alError != AL_SUCCESS)
	{
		fflush(stdout);
		al_utilities_debug_print_error("[DTNperf fatal error] in initiating memory for bundles");
		al_bundle_free(&bundle_received);
		return (ERROR);
	}

	sem_init(&csv_file_sem, 0, 1);

	// start expiration timer thread
	if (!oneCSVonly) //no need of timer in case of unique session
	{
		pthread_create(&session_exp_timer, NULL, session_expiration_timer, (void *) &monitor_options);
	}

	// start infinite loop
	while(1)
	{
		al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] waiting for bundles...\n");

		// wait until a bundle is received (the program can be terminated by a ctrl+c handler
		alError = al_socket_receive(registration_descriptor, &bundle_received, BP_PAYLOAD_MEM, BP_INFINITE_WAIT);

		switch (alError) {
		case AL_SUCCESS:
			dtnperf_error=processBundle (monitor_options);
			if (dtnperf_error==ERROR) {
				monitor_clean_exit(1, bundle_received, registration_descriptor);
			}
			break;
		case AL_WARNING_RECEPINTER:
			al_utilities_debug_print(DEBUG_L2, "Warning in al_socket_receive: Reception is interrupted \n");
			break;
		case AL_WARNING_TIMEOUT:
			al_utilities_debug_print(DEBUG_L2, "Warning in al_socket_receive: Timeout \n");
			break;
		default:
			fflush(stdout);
			al_utilities_debug_print_error("[DTNperf error] in getting recv reply: %d (%s)\n");
			monitor_clean_exit(1, bundle_received, registration_descriptor);
			break;
		}
	}//end while()
return (SUCCESS);
}//end function

void dtnperf_monitor_options_set_default(monitor_options_t *options) {

	options->expiration_session=120;
	options->oneCSVonly=FALSE;
	options->rtPrint=FALSE;
	options->rtPrintFile=stdout;

}

/******************************************************************************
 *
 * \par Function Name:
 *      monitorParsing
 *
 * \brief 	It parses the Monitor options (from the options not recognized by the
 * 			Unified API bundle parser)
 *
 * * \par Date Written:
 *  	15/02/20
 *
 * \return dtnperf_error_t					The error code
 *
 * \param[in]	unparsed_options			Options not recognized by the al_bp parser
 * \param[in]	registration_descriptor		Identifier of the registration
 * \param[out]	monitoR_options				The parsed Monitor options
 *
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | -----------------| -----------------------------------------------
 *  1/06/22  | B. Rogato  		| Initial Implementation of V4
 *  11/07/22 | C. Caini			| Revised to call the new "processBundle" function
 *****************************************************************************/

dtnperf_error_t monitorParsing(application_options_t unparsed_options, monitor_options_t *monitor_options){
	int optionIndex=0;
#define SKIP 1
#define HELP 2
#define	ONE_CSV_ONLY 3
#define RT_PRINT 4

	static struct option long_options[] =
			{
					{"monitor", no_argument, NULL, SKIP},
					{"help", no_argument, NULL, HELP},
					{"oneCSVonly", no_argument, NULL, ONE_CSV_ONLY},
					{"rt-print", optional_argument, NULL, RT_PRINT},
					{"session-expiration", required_argument, NULL,'e'},
					{NULL, 0, NULL, 0}	// The last element of the array has to be filled with zeros.
				};
	optind=1;

	while ((optionIndex = getopt_long(unparsed_options.app_opts_size, unparsed_options.app_opts, ":e:", long_options, NULL)) != -1) {
		switch (optionIndex) {
		case SKIP:
			//skip --monitor
			break;
		case HELP:
			printMonitorHelp(TRUE);
			return ERROR;
		case 'e':
			monitor_options->expiration_session=atoi(optarg);
			break;
		case ONE_CSV_ONLY:
			monitor_options->oneCSVonly=TRUE;
			break;
		case RT_PRINT: //rtPrint
			monitor_options->rtPrint=TRUE;
			if (optarg == NULL)
				break;
			FILE* f = fopen(optarg, "w+");
			if (f == NULL)
				{
					al_utilities_debug_print_error("[DTNperf error] error on opening rtPrint file %s: %s\n", optarg, strerror(errno));
					return ERROR;
				}
			else
				monitor_options->rtPrintFile = f;
			break;
		case ':':
			printMonitorHelp(FALSE);
			printf("DTNperf monitor error, missing argument for option: %s\n", unparsed_options.app_opts[optind-1]);
			return ERROR;
			break;
		case '?':
			printMonitorHelp(TRUE);
			printf("Dtnperf monitor parser error, option not recognized: %s\n", unparsed_options.app_opts[optind-1]);
			return ERROR;
			break;
		default:
			break;
		}
	}
return SUCCESS;
}

void printMonitorHelp(boolean_t al_bundle_options_help)
{
	printf("\nSyntax:\n");
	printf("--monitor [all options]\n\n");
	printf ("dtnperf monitor options:\n");
    printf (" -e, --session-expiration <s>  Max idle time of log files (s). Default: 120.\n");
    printf ("     --oneCSVonly              Generate a unique csv file\n");
    printf ("     --rt-print[=filename]     Print realtime human readable status report information\n");
    printf (" 	  --help                    This help.\n");
	if(al_bundle_options_help == TRUE){
		printf("%s\n", al_bundle_options_get_dtnsuite_help());
	}

}

/******************************************************************************
 *
 * \par Function Name:
 *      processBundle
 *
 * \brief 	It process the bundle read by the runMonitor. After classifying it, it prints
 * 			the relevant information as a raw of the corresponding session .csv file
 *
 * * \par Date Written:
 *  	10/07/22
 *
 * \return dtnperf_error_t					The error code
 *
 * \param[out]	monitor_options				The parsed Monitor options
 *
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | -----------------| -----------------------------------------------
 *  10/07/22 | C. Caini			| Initial implementation
 *****************************************************************************/

dtnperf_error_t processBundle (monitor_options_t monitor_options) {

	dtnperf_error_t dtnperf_error;
	al_types_bundle_status_report * status_report = NULL;
	bundle_type_t bundle_type=NONE;
	session_t * session;
	al_types_endpoint_id bundle_source_addr;
	al_types_creation_timestamp bundle_creation_timestamp, timestampForFilename;
	al_types_endpoint_id eidForFilename;
	dtnperfHeader_t dtnperf_header;
	struct timeval current;
	FILE * file=NULL;

	bundle_creation_timestamp.seqno=0;
	bundle_creation_timestamp.time=0;
	timestampForFilename.seqno=0;
	timestampForFilename.time=0;

	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] bundle received\n");
    // mark current time
	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] marking time...");
	gettimeofday(&current, NULL);
	//The function below extract the bundle parameters necessary to identify the session
	//It also identifies the type of bundle received (status report or STOP
	//If the type is status report, it also get the status report structure
	dtnperf_error=dtnperf_monitor_get_bundle_parameters(&status_report, &bundle_source_addr, &bundle_creation_timestamp,
			&dtnperf_header, &bundle_type);

	if(dtnperf_error==ERROR){ //fatal error
		al_utilities_debug_print_error("[DTNperf errors] in dtnperf_monitor_get_parameters \n");
		return ERROR;
	}
	if (dtnperf_error==WARNING){ //warning bundle unknown, skip the next lines and wait for a new bundle
		return WARNING;
	}

	// retrieve or open log file
	//take the mutex before writing
	sem_wait(&csv_file_sem);

	session = NULL;

	switch (bundle_type)
	{
	case STATUS_REPORT:
		if (monitor_options.rtPrint)
			printRealtimeStatusReport(monitor_options.rtPrintFile, bundle_source_addr, status_report);

		//copy the source eid which will be used to find the session (and the BSR creation timestamp )
		eidForFilename = status_report->bundle_x_id.source;
		timestampForFilename = status_report->bundle_x_id.creation_ts;

		//restituisce il puntatore alla struttura session_t a partire dal source id del bundle x e se non la trova la aggiunge
		session = session_get(session_list, eidForFilename);//session_list is global

		//se session è nuova crea il file e scrive la prima riga (intestazione)
		//restituisce sempre file descriptor
		if (session==NULL){
			create_new_session_file(bundle_type, current, &file, eidForFilename,
					timestampForFilename, bundle_creation_timestamp, status_report);
			session = session_get(session_list, eidForFilename); //session_list is global!
		}

		update_session(session, bundle_type, &file, bundle_creation_timestamp, status_report);
		print_BSR(current, *session->start, file, bundle_source_addr, bundle_creation_timestamp, *status_report);
		break;

	case CLIENT_STOP:
		//to identify the Client session to which this bundle stop refers
		eidForFilename = bundle_source_addr;
		timestampForFilename = bundle_creation_timestamp;

		//restituisce la struttura session_t
		session = session_get(session_list, eidForFilename);

		//se session è nuova crea il file e scrive la prima riga (intestazione)
		//restituisce sempre file descriptor
		if (session==NULL){
			create_new_session_file(bundle_type, current, &file, eidForFilename,
					timestampForFilename, bundle_creation_timestamp, status_report);
			session = session_get(session_list, eidForFilename); //session_list is global!
		}
		//Check if useful

		update_session(session, bundle_type, &file, bundle_creation_timestamp, status_report);
		print_bundleStop(current,*session->start,file,bundle_source_addr, bundle_creation_timestamp);

		if (!oneCSVonly)
			{
				session->total_to_receive = dtnperf_header.sent_bundles;
				al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] bundle stop arrived: bundle sent %u \n", session->total_to_receive);
//				session->wait_after_stop = monitor_options.expiration_session; //to be disabled
				session->wait_after_stop = dtnperf_header.lifetime;
				gettimeofday(session->stop_arrival_time, NULL);
				al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] bundle stop arrived: closing session in %u s MAX\n", session->wait_after_stop);
				//session_close(session_list, session);
			}
		break;
	default:
		break;
	}//switch
	sem_post(&csv_file_sem);
	return SUCCESS;
}

/******************************************************************************
 *
 * \par Function Name:
 *      dtnperf_monitor_get_bundle_parameters
 *
 * \brief 	It gets information about the received bundle (source, creation time, expiration, kind)
 *
 * * \par Date Written:
 *  	10/07/22
 *
 * \return dtnperf_error_t					The error code
 *
 * \param[out]	monitor_options				The parsed Monitor options
 *
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | -----------------| -----------------------------------------------
 *  10/07/22 | C. Caini			| Initial implementation
 *****************************************************************************/

dtnperf_error_t dtnperf_monitor_get_bundle_parameters(al_types_bundle_status_report ** status_report,
al_types_endpoint_id *bundle_source_eid, al_types_creation_timestamp *bundle_creation_timestamp,
dtnperfHeader_t *b_header, bundle_type_t *bundle_type){

	al_error error;
	dtnperf_error_t dtnperf_error;

	uint32_t payload_length=0;

/*
* Get session information from the header of the bundle received
*/
	//get bundle source-eid (to identify the session, i.e. the .csv file name where to write the collected information)
	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] get bundle source eid \n");
	error = al_bundle_get_source(bundle_received, bundle_source_eid);
	if (error != AL_SUCCESS)
	{
		fflush(stdout);
		al_utilities_debug_print_error ("[DTNperf fatal error] in getting bundle source-eid");
		return ERROR;
	}
	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] bundle source-eid = %s\n\n", bundle_source_eid->uri);

	// get bundle creation timestamp (to identify the session)
	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2]\t get bundle creation timestamp \n");
	error = al_bundle_get_creation_timestamp(bundle_received, bundle_creation_timestamp);
	if (error != AL_SUCCESS)
	{
		fflush(stdout);
		al_utilities_debug_print_error("[DTNperf fatal error] in getting bundle creation timestamp");
		return ERROR;
	}
	al_utilities_debug_print(DEBUG_L2,"\tbundle creation timestamp:\n"
			"\ttime = %lu\n\tseqno= %lu\n",
			(unsigned int) bundle_creation_timestamp->time, bundle_creation_timestamp->seqno);

/*
 * Try to recognize the type of the bundle received
 */
	//check if is a BSR and if yes get information and return success
	// try to get status report (BSR) information from the received bundle
	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] try to get status report information if any from the received bundle\n ");
	error = al_bundle_get_status_report(bundle_received, status_report);
	//if OK status_report is the structure containing BSR
	if (error == AL_SUCCESS){
			*bundle_type = STATUS_REPORT;
			al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] status report received, information parsed.   \n");
			return SUCCESS;
	}
	//otherwise check if is a Bundle stop and if yes return success
	//try to get the DTNperf header
	dtnperf_error=getDtnperfHeader(bundle_received, b_header, payload_length);
	if (dtnperf_error == SUCCESS && b_header->type == 3) {
		*bundle_type = CLIENT_STOP;
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] DTNperf Bundle stop received. \n");
		return SUCCESS;
	}
	//otherwise check if is an IRFTODO
	//otherwise the bundle is uknown
	al_utilities_debug_print_error("[DTNperf warning] Unknown-type bundle received\n");
	return WARNING;
}

/*
 * Functions related to application stop
 */

void closing_monitor_handler(int signal)
{
		printf("\nDtnperf monitor: received SIGINT (ctrl+C). Exiting\n");
		monitor_clean_exit(0, bundle_received, 1); //the last "1" is a temporary fix; it should be the registration descriptor
}

void monitor_clean_exit(int status, al_types_bundle_object bundle_received, al_socket_registration_descriptor registration_descriptor)
{
	session_t * session;

	//set TRUE the variable forced close, which closes the session_expiration_timer thread
	set_closing_monitor(TRUE);

	while (session_list->first != NULL)
	{
		session = session_list->first;
		session_close(session_list, session);
	}

	al_bundle_free(&bundle_received);
	session_list_destroy(session_list);

	al_socket_unregister(registration_descriptor);
	al_socket_destroy();
	al_utilities_debug_print_debugger_destroy();
	printf("Dtnperf Monitor: exit.\n");
	exit(status);
}

boolean_t get_closing_monitor()
{
	int result;
	pthread_mutex_lock(&mutex_forced_close);
	result = forced_close;
	pthread_mutex_unlock(&mutex_forced_close);
	return result;
}

void set_closing_monitor(boolean_t value)
{
	pthread_mutex_lock(&mutex_forced_close);
	forced_close = value;
	pthread_mutex_unlock(&mutex_forced_close);
}

/*
 * High level printing functions
 */
void print_BSR(struct timeval current, struct timeval start,
		FILE * file, al_types_endpoint_id bundle_source_addr, al_types_creation_timestamp bundle_creation_timestamp,
		al_types_bundle_status_report status_report) {

	    // print BSR Rx time in .csv file
		csv_print_rx_time(file, current, start);
		// print BSR source in .csv file
		csv_print_eid(file, bundle_source_addr);
		//print BSR creation timestamp in csv file
		csv_print_timestamp(file, bundle_creation_timestamp);
		// print BSR type in .csv file
		csv_print(file, "STATUS_REPORT");
		// print BSR bundle_x (5 fields)
		csv_print_bundle_x_information(file, status_report);
}

void print_bundleStop(struct timeval current, struct timeval start,
		FILE * file, al_types_endpoint_id bundle_source_addr, al_types_creation_timestamp bundle_creation_timestamp) {
	// print rx time in csv log
	csv_print_rx_time(file, current, start);
	// print bundle source in csv log
	csv_print_eid(file, bundle_source_addr);
	//print bundle creation timestamp in csv log
	csv_print_timestamp(file, bundle_creation_timestamp);
	// print bundle type in csv log
	csv_print(file, "CLIENT_STOP");
	csv_print(file, " ; ; ; ; ; ; ; ; ; ; ;");
	// end line in csv log
	csv_end_line(file);
}


/*
 * Functions related to session management
 */

void create_new_session_file(bundle_type_t bundle_type, struct timeval current,
		FILE ** file, al_types_endpoint_id bundle_x_source_eid, al_types_creation_timestamp bundle_x_creation_timestamp,
		al_types_creation_timestamp bundle_creation_timestamp, al_types_bundle_status_report * status_report){

	char temp[256];
	char * filename;
	char * full_filename;
	int filename_len;
	session_t * session;

	//First step: build the full filename (including path)
	if (oneCSVonly) //one file
	{
		//build filename
		sprintf(temp, "%lu_%s", bundle_x_creation_timestamp.time, MONITOR_UNIQUE_CSV_FILENAME);
		full_filename = (char *) malloc(strlen(MONITOR_LOGS_DIR_DEFAULT) + strlen(temp) + 2);
		sprintf(full_filename, "%s/%s", MONITOR_LOGS_DIR_DEFAULT, temp);
	}
	else //multiple files (sessions);  build the file name as timestamp_shortenedEid
	{
		//calculate the filename length
		if(strncmp(bundle_x_source_eid.uri,"ipn",3) == 0) //if 0 the scheme is ipn
		{
			filename_len = strlen(bundle_x_source_eid.uri) - strlen("ipn:") + 24;
		}
		else //dtn
		{
			filename_len = strlen(bundle_x_source_eid.uri) - strlen("dtn://") + 24;
		}

		//insert the timestamp of bundle_x (the data bundle to which the first BSR refers)
		filename = (char *) malloc(filename_len);
		memset(filename, 0, filename_len);
		sprintf(filename, "%lu_", bundle_x_creation_timestamp.time); //controllare lu
		memcpy(temp, bundle_x_source_eid.uri, strlen(bundle_x_source_eid.uri) + 1);
		//add the shortened bundle_x source eid
		if(strncmp(bundle_x_source_eid.uri,"ipn",3) == 0)// if 0 the scheme is ipn
		{
			strtok(temp, ":"); //get what follows the ":" delimiter, i.e. the service number
			strcat(filename, strtok(NULL, "\0")); //add the terminator
		}
		else
		{
			char * ptr;
			strtok(temp, "/");
			strcat(filename, strtok(NULL, "/"));
			// remove .dtn suffix from the filename
			ptr = strstr(filename, ".dtn");
			if (ptr != NULL)
				ptr[0] = '\0';
		}
		//add the extension
		strcat(filename, ".csv");
		//build the full_filename including the path
		full_filename = (char *) malloc(strlen(MONITOR_LOGS_DIR_DEFAULT) + strlen(filename) + 2);
		sprintf(full_filename, "%s/%s", MONITOR_LOGS_DIR_DEFAULT, filename);
		free (filename);
	}//first step end

	//second step: open the file in append mode
	*file = fopen(full_filename, "a");
	if(file==NULL){
		al_utilities_debug_print(DEBUG_L1,"Error: can't open the file! errno: %d\n", errno);
	}

	if (oneCSVonly) //unique session, unique file
		session = unique_session_create(full_filename, *file, current, bundle_x_creation_timestamp.time);
	else // multiple session (files)
		session = session_create(bundle_x_source_eid, full_filename, *file, current, bundle_x_creation_timestamp.time);

	session_put(session_list, session);//Unificare e mettere dentro

	// third step: write header in csv log file
	csv_print_status_report_timestamps_header(*file);
	csv_end_line(*file);
	free (full_filename);
} //end new session, if(session==null)

void update_session(session_t * session, bundle_type_t bundle_type,
		FILE ** file, al_types_creation_timestamp bundle_creation_timestamp, al_types_bundle_status_report * status_report){
/*
 *  Update session info and increase the counter
 */
	// update session info
	//bundle_creation timestamp e' l'istante al quale e' stato creato il BSR (o altro bundle da inserire nei log)
	session->expiration = MONITOR_DEFAULT_SESSION_EXPIRATION;//CCaini modified; work in progress ...
	session->last_bundle_time = bundle_creation_timestamp.time;


	*file = session->file;
//	memcpy(start, session->start, sizeof(struct timeval));

    //se il bundle e' un BSR delivered incrementa il contatore
	if (bundle_type == STATUS_REPORT && (status_report->flags & BP_STATUS_DELIVERED))
	{
		session->delivered_count++;
	}

}

// session expiration timer thread
void * session_expiration_timer(void * opt)
{
	uint32_t current_dtn_time;
	struct timeval current_time;
	session_t * session, * next;

	while(!get_closing_monitor())
	{
		current_dtn_time = get_current_dtn_time();
		gettimeofday(&current_time, NULL);

		sem_wait(&csv_file_sem);

		for(session = session_list->first; session != NULL; session = next)
		{
			next = session->next;
			// all status reports has been received: close session
			if (session->total_to_receive > 0 && session->delivered_count == session->total_to_receive &&
					// wait 3 seconds before closing the session
					session->stop_arrival_time->tv_sec + 3 < current_time.tv_sec)
			{
				session_close(session_list, session);
			}

			// stop bundle arrived but not all the status reports have arrived and the timer has expired
			else if (session->stop_arrival_time->tv_sec != 0 &&
					session->total_to_receive > 0 && session->stop_arrival_time->tv_sec + session->wait_after_stop < current_time.tv_sec)
			{
				fprintf(stdout, "DTNperf monitor: Session Expired: Bundle stop arrived, but not all the status reports did\n");
				fprintf(stdout, "\tsaved log file: %s\n\n", session->full_filename);
				if (fclose(session->file) < 0)
					perror("[DTNperf warning] closing expired file:");
				session_del(session_list, session);
			}
			// stop bundle has not arrived yet and the last bundle has expired
			else if (session->stop_arrival_time->tv_sec == 0 &&
					session->last_bundle_time + session->expiration + 2 < current_dtn_time && (session->last_bundle_time + session->expiration != 0))
			{
				fprintf(stdout, "DTNperf monitor: Session Expired: Bundle stop did not arrive\n");
				fprintf(stdout,"\tsaved log file: %s\n\n", session->full_filename);
				if (fclose(session->file) < 0)
					perror("[DTNperf warning] closing expired file:");
				session_del(session_list, session);
			}
		}
		sem_post(&csv_file_sem);
		sched_yield();
		sleep(1);
	}
	pthread_exit(NULL);
}

void session_close(session_list_t * list, session_t * session)
{
	fclose(session->file);
	fprintf(stdout, "DTNperf monitor: saved log file: %s\n\n", session->full_filename);
	session_del(session_list, session);
}

session_list_t * session_list_create()
{
	session_list_t * list;
	list = (session_list_t *) malloc(sizeof(session_list_t));
	list->first = NULL;
	list->last = NULL;
	list->count = 0;
	return list;
}

void session_list_destroy(session_list_t * list)
{
	free(list);
}

session_t * session_create(al_types_endpoint_id client_eid, char * full_filename, FILE * file, struct timeval start,
		uint64_t bundle_timestamp_time)
//Rivedere con Belano
{
	session_t * session;
	session = (session_t *) malloc(sizeof(session_t));
	session->start = (struct timeval *) malloc(sizeof(struct timeval));
	session->stop_arrival_time = (struct timeval *) malloc(sizeof(struct timeval));
	session->stop_arrival_time->tv_sec = 0;
	session->stop_arrival_time->tv_usec = 0;
	session->client_eid=client_eid;
	session->full_filename = strdup(full_filename);
	session->file = file;
	memcpy(session->start, &start, sizeof(struct timeval));
	session->last_bundle_time = bundle_timestamp_time;
	session->expiration = MONITOR_DEFAULT_SESSION_EXPIRATION;
	session->delivered_count = 0;
	session->total_to_receive = 0;
	session->wait_after_stop = 0;
	session->next = NULL;
	session->prev = NULL;
	return session;
}

session_t * unique_session_create(char * full_filename, FILE * file, struct timeval start, uint64_t bundle_timestamp_time)
{
	session_t * session;
	session = (session_t *) malloc(sizeof(session_t));
	session->start = (struct timeval *) malloc(sizeof(struct timeval));
	session->stop_arrival_time = (struct timeval *) malloc(sizeof(struct timeval));
	session->full_filename = strdup(full_filename);
	session->file = file;
	memcpy(session->start, &start, sizeof(struct timeval));
	session->last_bundle_time = bundle_timestamp_time;
	session->expiration = 0; //unique session never expires
	session->delivered_count = 0;
//	session->total_to_receive = 0; //this is useless
//	session->wait_after_stop = 0; //this is useless
	session->next = NULL;
	session->prev = NULL;
	return session;
}

void session_destroy(session_t * session)
{
	free(session->start);
	free(session->stop_arrival_time);
	free(session->full_filename);
	free(session);
}

void session_put(session_list_t * list, session_t * session)
{
	if (list->first == NULL) // empty list
	{
		list->first = session;
		list->last = session;
	}
	else
	{
		session->prev = list->last;
		list->last->next = session;
		list->last = session;
	}
	list->count ++;

}

session_t *  session_get(session_list_t * list, al_types_endpoint_id client)
{
	session_t * item = list->first;
	// if oneCSVonly return the unique session (NULL if it isn't created yet)
	if (oneCSVonly)
		return item;

	while (item != NULL)
	{
		if (strcmp(item->client_eid.uri, client.uri) == 0)
		{
			return item;
		}
		item = item->next;
	}
	return NULL;
}

void session_del(session_list_t * list, session_t * session)
{
	if (session->next == NULL && session->prev == NULL) // unique element of the list
	{
		list->first = NULL;
		list->last = NULL;
	}
	else if (session->next == NULL) // last item of list
	{
		list->last = session->prev;
		session->prev->next = NULL;
	}
	else if (session->prev == NULL) // first item of the list
	{
		list->first = session->next;
		session->next->prev = NULL;
	}
	else // generic element of list
	{
		session->next->prev = session->prev;
		session->prev->next = session->next;
	}
	session_destroy(session);
	list->count --;
}







