/** \file status_report_print.c
 *
 *  \brief  This file contains low level functions to print BSR or STOP bundle information into a line of the .csv file
 *
 *  \details All functions are very low level. They derive from DTNperf_3 version with few modifications
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **
 *\authors
 *  	Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *      Anna d'Amico, anna.damico@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/
#include <stdlib.h>
#include "status_report_print.h"
#include "../../utils.h" //CCaini because of sub_time

/**
 * \brief Returns a string version of a status report type.
 */

char* status_report_flag_to_str(al_types_status_report_flags flag)
{
	static char temp[256];
	temp[0] = '\0';
	if (flag & BP_STATUS_RECEIVED)
		strcat(temp, "RECEIVED, ");
	if (flag & BP_STATUS_CUSTODY_ACCEPTED)
		strcat(temp, "CUSTODY_ACCEPTED, ");
	if (flag & BP_STATUS_FORWARDED)
		strcat(temp, "FORWARDED, ");
	if (flag & BP_STATUS_DELIVERED)
		strcat(temp, "DELIVERED, ");
	if (flag & BP_STATUS_DELETED)
		strcat(temp, "DELETED");
	temp[strlen(temp) - 2] = '\0';
	return temp;
}

/**
 * \brief Returns a string version of a status report reason.
 */

const char* status_report_reason_to_str(al_types_status_report_reason err)
{
	switch (err) {
	case BP_SR_REASON_NO_ADDTL_INFO:
		return "no additional information";

	case BP_SR_REASON_LIFETIME_EXPIRED:
		return "lifetime expired";

	case BP_SR_REASON_FORWARDED_UNIDIR_LINK:
		return "forwarded over unidirectional link";

	case BP_SR_REASON_TRANSMISSION_CANCELLED:
		return "transmission cancelled";

	case BP_SR_REASON_DEPLETED_STORAGE:
		return "depleted storage";

	case BP_SR_REASON_ENDPOINT_ID_UNINTELLIGIBLE:
		return "endpoint id unintelligible";

	case BP_SR_REASON_NO_ROUTE_TO_DEST:
		return "no known route to destination";

	case BP_SR_REASON_NO_TIMELY_CONTACT:
		return "no timely contact";

	case BP_SR_REASON_BLOCK_UNINTELLIGIBLE:
		return "block unintelligible";

	default:
		return "(unknown reason)";
	}
}

void csv_print_rx_time(FILE * file, struct timeval time, struct timeval start_time)
{
	struct timeval * result = malloc(sizeof(struct timeval));//CCaini and the free?
	char buf[50];
	sub_time(time, start_time, result);
	sprintf(buf, "%ld.%ld;", result->tv_sec, result->tv_usec);
	free (result);
	fwrite(buf, strlen(buf), 1, file);
}

void csv_print_eid(FILE * file, al_types_endpoint_id eid)
{
	char buf[257];
	sprintf(buf, "%s;", eid.uri);
	fwrite(buf, strlen(buf), 1, file);
}

void csv_print_timestamp(FILE * file, al_types_creation_timestamp timestamp)
{
	char buf[50];
	sprintf(buf, "%lu;%lu;", timestamp.time, timestamp.seqno);
	fwrite(buf, strlen(buf), 1, file);
}

void csv_print_status_report_timestamps_header(FILE * file)
{
	fprintf(file,"RX_TIME;Report_SRC;Report_TST;Report_SQN;"
			"Report_Type;Bndl_SRC;Bndl_TST;Bndl_SQN;"
			"Bndl_FO;Bndl_FL;Dlv;Ct;Rcv;Fwd;Del;Reason;");
}

void csv_print_bundle_x_information(FILE * file, al_types_bundle_status_report status_report){

	//Print the 5 fields of bundle_x
	csv_print_eid(file, status_report.bundle_x_id.source);
	csv_print_timestamp(file, status_report.bundle_x_id.creation_ts);
	csv_print_ulong(file, status_report.bundle_x_id.frag_offset);
	csv_print_ulong(file, status_report.bundle_x_id.frag_length);
    //Print all timestamps and the reason code
	csv_print_status_report_timestamps(file, status_report);
	csv_print(file, " ;");
	csv_end_line(file);
}

void csv_print_status_report_timestamps(FILE * file, al_types_bundle_status_report status_report)
{
	char buf1[256];
	char buf2[50];
	memset(buf1, 0, 256);

	if (status_report.flags & BP_STATUS_DELIVERED)
		sprintf(buf2, "%lu;", status_report.delivery_ts);
	else
		sprintf(buf2, " ; ");
	strcat(buf1, buf2);

	if (status_report.flags & BP_STATUS_CUSTODY_ACCEPTED)
		sprintf(buf2, "%lu;", status_report.custody_ts);
	else
		sprintf(buf2, " ;");
	strcat(buf1, buf2);

	if (status_report.flags & BP_STATUS_RECEIVED)
		sprintf(buf2, "%lu;", status_report.reception_ts);
	else
		sprintf(buf2, " ;");
	strcat(buf1, buf2);

	if (status_report.flags & BP_STATUS_FORWARDED)
		sprintf(buf2, "%lu;", status_report.forwarding_ts);
	else
		sprintf(buf2, " ;");
	strcat(buf1, buf2);

	if (status_report.flags & BP_STATUS_DELETED)
		sprintf(buf2, "%lu;", status_report.deletion_ts);
	else
		sprintf(buf2, " ;");
	strcat(buf1, buf2);

	// status report reason
	strcat(buf1, status_report_reason_to_str(status_report.reason));
	strcat(buf1, ";");

	fwrite(buf1, strlen(buf1), 1, file);
}

void csv_print_ulong(FILE * file, unsigned long num)
{
	char buf[50];
	sprintf(buf, "%lu", num);
	csv_print(file, buf);
}

void csv_print(FILE * file, char * string)
{
	char buf[256];
	memset(buf, 0, 256);
	strcat(buf, string);
	if (buf[strlen(buf) -1] != ';')
		strcat(buf, ";");
	fwrite(buf, strlen(buf), 1, file);
}

void csv_end_line(FILE * file)
{
	char c = '\n';
	fwrite(&c, 1, 1, file);
	fflush(file);
}

//secondo me si può usare quella dell'al_bp
int bundle_id_sprintf(char * dest, al_types_bundle_id * bundle_id)
{
	char offset[16], length[16];
	if (bundle_id->frag_offset != 0)
	{
		sprintf(offset, "%d", bundle_id->frag_offset);
	}
	else
	{
		offset[0] = '\0';
	}
	if (bundle_id->frag_length != 0)
	{
		sprintf(length, "%d", bundle_id->frag_length);
	}
	else
	{
		length[0] = '\0';
	}
	return sprintf(dest, "%s, %lu.%lu,%s,%s", bundle_id->source.uri, bundle_id->creation_ts.time,
			bundle_id->creation_ts.seqno, offset, length);
}

void printOneLineRealtimeStatusReport(FILE *f, al_types_endpoint_id sr_source, al_types_bundle_status_report * status_report,
		al_types_status_report_flags type, al_types_timeval ts)
//It prints one BSR line, i.e. either delivered or received, etc.
{
	char bundle_id[256];
	bundle_id_sprintf(bundle_id, &(status_report->bundle_x_id));
	fprintf(f, "SR %s at %lu, from node %s, referring to bundle (%s) %s\n", status_report_flag_to_str(type),
			ts, sr_source.uri, bundle_id,
			status_report_reason_to_str(status_report->reason));
}

void printRealtimeStatusReport(FILE *f, al_types_endpoint_id sr_source, al_types_bundle_status_report * status_report)
//One BSR may reports multiple information items, i.e. both delivered and received
//This function prints a line for each item
{
	if (status_report->flags & BP_STATUS_RECEIVED)
		printOneLineRealtimeStatusReport(f, sr_source, status_report, BP_STATUS_RECEIVED, status_report->reception_ts);
	if (status_report->flags & BP_STATUS_CUSTODY_ACCEPTED)
		printOneLineRealtimeStatusReport(f, sr_source, status_report, BP_STATUS_CUSTODY_ACCEPTED, status_report->custody_ts);
	if (status_report->flags & BP_STATUS_FORWARDED)
		printOneLineRealtimeStatusReport(f, sr_source, status_report, BP_STATUS_FORWARDED, status_report->forwarding_ts);
	if (status_report->flags & BP_STATUS_DELIVERED)
		printOneLineRealtimeStatusReport(f, sr_source, status_report, BP_STATUS_DELIVERED, status_report->delivery_ts);
	if (status_report->flags & BP_STATUS_DELETED)
		printOneLineRealtimeStatusReport(f, sr_source, status_report, BP_STATUS_DELETED, status_report->deletion_ts);
	fflush(f);

}




