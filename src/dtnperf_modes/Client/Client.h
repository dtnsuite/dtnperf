/** \file Client.h
 *
 *  \brief  This file contains the prototypes of functions defined in Client.c.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#ifndef CLIENT_H
#define CLIENT_H


#include "../../dtnperf_system_libraries.h"
#include <unified_api.h>



//#include "ClientMode/ClientTimeMode/ClientTimeMode.h"
//#include "ClientMode/ClientDataMode/ClientDataMode.h"
//#include "ClientMode/ClientFileMode/ClientFileMode.h"
//#include "CongestionControl/CongestionControlRate/CongestionControlRate.h"
//#include "CongestionControl/CongestionControlWindow/CongestionControlWindow.h"
//#include "CongestionControl/CongestionControlWindow/CongestionWindow.h"
//#include "Sender/Sender.h"
//#include "ClientPayloadManager/ClientPayloadManager.h"
//#include "ClientMode/ClientMode.h"


int isCtrlcReceived();
int isSenderRunning();
int terminationThresholdReached();
void prepareNextBundle(al_types_bundle_object* bundle);
void setClientBundleHeader();
int runClient(bundle_options_t bundle_options,
		dtn_suite_options_t dtnsuite_options,
		application_options_t unparsed_options);

int incBundleSent(al_types_endpoint_id source, al_types_creation_timestamp creationTs);
void incTotalDataSent(long dataSent);
void passBundlePayloadToClient(int parameter);
void setFileModeOverhead(int overheadParameter);
void cleanExit();

#endif
