/** \file CongestionControlRate.c
 *
 *  \brief  This file contains the Congestion Control Rate functions of the DTNperf v4 Client. The CC aim is to let the Sender to
 *  send bundles only when allowed by the Congestion Control thread, following a producer-consumer model.
 *  There are two possible congestion control algorithms: Window and Rate.
 *  In this file there are the functions specific to Rate congestion control.
 *  Window and Rate variants are implemented by using polymorphism.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of the present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <stdio.h>
#include <unistd.h>

#include "CongestionControlRate.h"
#include "../../Client.h"

static double period;

long microsOf(double seconds);

/**
 * \brief Set the congestion control to operate in the specific rate modality, using polymorfism:
 * Pointers of CongestionControl data structure are pointed to the specific attributes and real function of a 
 * congestion control that works in Rate modality.
 * 
 * \par Function name:
 *      setCongestionControlRate
 * 
 * \par Date Written:
 *      05/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  05/11/20 | A. Zappacosta   |   Initial declaration 
*/
void setCongestionControlRate(CongestionControl* congestionControl, double rate, RateType rateType, long bundleDim){
//CCaini A che serve impostare i congestion control number, rateType ecc.
//Se serve a qualcosa andrebbe impostata solo la rate in bundle/s

	congestionControl->type = CongestionControlRateType;
//    congestionControl->congestionControlRate.number = rate;
//    congestionControl->congestionControlRate.type = rateType;
    if(rateType == BundlePerSecond){
    	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Congestion control = Rate, Rate = %lf bundle/s \n", rate);
    	//do nothing: rate already in in bundle/s
  }
    else if(rateType == BitPerSecond){
    	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Congestion control = Rate, Rate = %lf kbit/s \n", rate);
        rate=rate/(bundleDim*8); //rate converted in bundle/s
    }
    period=1/rate;
    al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Bundle interval : %.4f s\n", period);
//    congestionControl->waitForNext = waitForNextCC_Rate; //set waitForNext at waitForNextCC_Rate() (Polymorphic)
}

/**
 * \brief The caller (sender) of this function put itself in a blocking wait for the time needed to maintain the choose bundle sending rate.
 * The blocking wait is performed by a sleep.
 * 
 * \par Function name:
 *      waitForNextCC_Rate
 * 
 * \par Date Written:
 *      05/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  05/11/20 | A. Zappacosta   |   Initial declaration 
*/
void waitForNextCC_Rate(){
    sleep(period);
    usleep(microsOf(period));
}

void* _mainCongestionControlRate(void* parm) {
    while (isSenderRunning()){
//client running false should result into an immediate exit
//Otherwise stop as soon as the sender stops
        waitForNextCC_Rate();
        congestionControlSemPost();
    }
  return NULL;
}

long microsOf(double seconds){
    double dec = seconds - (int) seconds;
    long result = dec * 1000000;
    return result;
}

