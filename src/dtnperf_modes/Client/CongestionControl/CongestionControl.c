/** \file CongestionControl.c
 *
 *  \brief  This file contains the Congestion Control thread functions of the DTNperf v4 Client. The CC aim is to let the Sender to
 *  send bundles only when allowed by the Congestion Control thread, following a producer-consumer model.
 *  There are two possible congestion control algorithms: Window and Rate.
 *  Functions specific to Rate or Window congestion controls are in separate directories.
 *  Window and Rate variants are implemented by using polymorphism.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <stdio.h>
#include <unistd.h>
#include <signal.h>

#include "CongestionControl.h"

#include "CongestionControlRate/CongestionControlRate.h"
#include "CongestionControlWindow/CongestionControlWindow.h"
#include "CongestionControlWindow/CongestionWindow.h"

#include "../Client.h"

static sem_t semaphore;
static pthread_t congestionControlThread;

void congestionControlSemPost(){
	sem_post(&semaphore);
}


void waitForStopCongestionControl(){
    pthread_join(congestionControlThread, NULL);
    sem_destroy(&semaphore);
}



/**
 * \brief Start the _mainCongestionControl thread method initializing semaphore to the value of:
 *  - 0 if just one bundle on flight is allowed (Rate based Congestion Control)
 *  - windowSize if (windowSize - 1) bundles in flight are allowed (Window based Congestion Control)
 *  - 0 and W-1  because the semaphore is checked only after the first bundle sent
 *
 * \par Function name:
 *      startCongestionControl
 * 
 * \par Date Written:
 *      03/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  03/11/20 | A. Zappacosta   |   Initial implementation and documentation
 *  12/11/20 | A. Zappacosta   |   W provided through a windowSize starting value of the semaphore
*/
void startCongestionControl(CongestionControl* settedCongestionControl) {
    if(settedCongestionControl->type == CongestionControlRateType){
    	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Rate based congestion control started \n");
        sem_init(&semaphore, 0, 0);
        pthread_create(&congestionControlThread, NULL, _mainCongestionControlRate, NULL);
    }
    else  if(settedCongestionControl->type == CongestionControlWindowType){
    	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Window based congestion control started\n");
        sem_init(&semaphore, 0, (settedCongestionControl->congestionControlWindow.window - 1));
        pthread_create(&congestionControlThread, NULL, _mainCongestionControlWindow, NULL);
    }
}

/**
 * \brief Sending a bundle will be allowed when semaphore has value >= 1
 * 
 * \par Function name:
 *      waitToSend
 * 
 * \par Date Written:
 *      05/11/20
 *\
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  05/11/20 | A. Zappacosta   |   Initial implementation
*/
void waitToSend(){
    sem_wait(&semaphore);
}

/**
 * \brief Unlock the wait function by increasing the semaphore by one).
 * 
 * \par Function name:
 *      stopCongestionControl
 * 
 * \par Date Written:
 *      03/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  03/11/20 | A. Zappacosta   |   Initial implementation and documentation 
*/
void unlockWaitToSend(){
    sem_post(&semaphore);
}

