/* \file CongestionControl.h
 *
 *  \brief  This file contains the prototypes of functions defined in CongestionControl.h.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#ifndef CONGESTIONCONTROL_H
#define CONGESTIONCONTROL_H

#include <pthread.h>
#include <semaphore.h>

typedef enum {
  CongestionControlRateType, 
  CongestionControlWindowType
} CongestionControlType;

typedef enum {
  BitPerSecond,
  BundlePerSecond
} RateType;

typedef struct {
  double number;
  RateType type;
} CongestionControlRate;

typedef struct {
  int window;
} CongestionControlWindow;


typedef struct {
  CongestionControlType          type;
  
  union {
    CongestionControlRate      congestionControlRate;
    CongestionControlWindow      congestionControlWindow;
  };
} CongestionControl;


void startCongestionControl(CongestionControl* settedCongestionControl);
void unlockWaitToSend();
void waitToSend();
void waitForStopCongestionControl();
void congestionControlSemPost();

CongestionControlType getCongestionControlType();

#endif
