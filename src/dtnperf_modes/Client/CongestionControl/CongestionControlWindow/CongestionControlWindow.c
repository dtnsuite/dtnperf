/* \file CongestionControlWindow.c
 *
 *  \brief  This file contains the Congestion Control Window functions of the DTNperf v4 Client. The CC aim is to let the Sender to
 *  send bundles only when allowed by the Congestion Control thread, following a producer-consumer model.
 *  There are two possible congestion control algorithms: Window and Rate.
 *  In this file there are the functions specific to Window congestion control.
 *  Window and Rate variants are implemented by using polymorphism.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of the present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

#include <unified_api.h>

#include "CongestionControlWindow.h"
#include "CongestionWindow.h"
#include "../../Sender/Sender.h"
#include "../../../Client/Client.h"
#include "../../../../dtnperfHeaders.h"
#include "../../../../dtnperf_definitions.h"


static al_socket_registration_descriptor registration_descriptor;
static al_types_bundle_object bundleAck;
static struct BundleInFlight* headOfWindowList;
static unsigned int ackReceveingTimeoutInSeconds;
static struct timeval currentTimestamp;
static struct timeval receivingAckTimeDeadline;
static uint64_t TIMEOUT_RECEIVE_MS = 1000; //in ms


/**
 * \brief Set the congestion control to operate in the specific window modality, using polymorfism:
 * pointers of CongestionControl datastructure are pointed to the specific data and the real functions of a 
 * congestion control working in Window modality.
 * 
 * \par Function name:
 *      setCongestionControlRate
 * 
 * \par Date Written:
 *      05/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  06/11/20 | A. Zappacosta   |   Initial implementation 
*/
void setCongestionControlWindow(CongestionControl* congestionControl, int windowSize){

	congestionControl->type = CongestionControlWindowType;
    congestionControl->congestionControlWindow.window = windowSize;

    headOfWindowList = NULL;

    ackReceveingTimeoutInSeconds = INTER_ACK_TIMEOUT; //as the previous version of DTNperf
    al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Congestion control = window, window size is %d \n", windowSize);

}

/**
 * \brief Interface that could be used to debug print the whole congestion window
 *  at a specific instant.
 * 
 * \par Function name:
 *      printCongestionWindow
 * 
 * \par Date Written
 *      21/04/2021
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  21/04/21 | A. Zappacosta   |   Implementation 
*/
void printCongestionWindow(){
    printWindow(headOfWindowList);
}

/**
 * \brief Function to receive a bundle ACK from server. In particular:
 * - if a bundle ACK is received and its unique identifier (source, creation_ts) is
 * among the list of bundle sent, the function frees a place in the congestion window
 * 
 * - if a bundle that is not a bundle ACK is received, or the received bundle ACK does not refer to any of bundle sent,
 * it is discarded and a new receiving cycle is started
 * 
 * - if timeout occurs, the whole application is stopped
 * 
 * \par Function name:
 *      waitForNextCC_Window
 * 
 * \par Date Written:
 *      05/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  06/11/20 | A. Zappacosta   |   Initial implementation
 *  21/04/21 | A. Zappacosta   |   Interfacing with CongestionWindow
 *  18/02/22 | A. Belano	   |   Adapted to the new header
 *  01/06/22 | C. Caini 	   |   ACK structure reused instead of being created at each cycle
*/

void waitForNextCC_Window(){

    DeletionResult foundBundleIdentifierInCW = UNSUCCESSFULL_DELETION;
    al_error result;
    dtnperfHeader_t ackHeader;

    al_types_endpoint_id sourceAckEid;

    uint32_t sizeOfAck;
    int i1, i2; //temporary, for debug


    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Entered waitForNextCC_Window...\n");
    gettimeofday(&currentTimestamp, NULL);
    receivingAckTimeDeadline = currentTimestamp;
    receivingAckTimeDeadline.tv_sec += (ackReceveingTimeoutInSeconds);

	while(isSenderRunning() || ((getNumBundleInFlight() > 0) && !isCtrlcReceived())) {
		al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Starting waiting for bundle ACK(s)...\n");
    	result = al_socket_receive(registration_descriptor, &bundleAck, BP_PAYLOAD_MEM, TIMEOUT_RECEIVE_MS);
    	if(result == AL_SUCCESS){ //a bundle has arrived before 1s al_socket_receive timeout
    		al_bundle_get_payload_size(bundleAck, &sizeOfAck);
    		result = getDtnperfHeader(bundleAck, &ackHeader, sizeOfAck);
    		if (result == AL_SUCCESS && ackHeader.type == 2) {// the bundle arrived is a dtnperf ACK
    			al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] ACK received: %s - %lu.%lu\n", ackHeader.bundlex_src_eid, ackHeader.bundlex_timestamp.time, ackHeader.bundlex_timestamp.seqno);    			strcpy(sourceAckEid.uri, ackHeader.bundlex_src_eid);
    			al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Check if received ACK refers to one of the bundles in flight...");
    			foundBundleIdentifierInCW = deleteBundleInFlight(&headOfWindowList, sourceAckEid, ackHeader.bundlex_timestamp);
    			if (foundBundleIdentifierInCW == SUCCESSFULL_DELETION) {//CCaini TRUE FALSE
    				al_utilities_debug_print(DEBUG_L2,"Bundle successfully acknowledged\n");
    				break;
    			} else {
    				al_utilities_debug_print(DEBUG_L2,"The received ACK does not refer to any bundles in flight\n");
    			}
    		}
			free(ackHeader.bundlex_src_eid); //free as it is created with a malloc calling getDtnperfHeader
    	} else if (result== AL_WARNING_RECEPINTER || result ==AL_WARNING_TIMEOUT) {
   //CCaini The call to the al_utilities_debug_print was an attempt to avoid in DTN2 an immediate relock of the mutex
    		//that is used to half-duplexing send and receive
 //   		al_utilities_debug_print(DEBUG_L2,"[WARNING]: timeout: no ack in the last %u s \n", 1);
  		//check if CC timeout has fired; if yes exit, otherwise do nothing
 //   		usleep(100);
 //   		sched_yield();
    		gettimeofday(&currentTimestamp, NULL);
    		if(currentTimestamp.tv_sec > receivingAckTimeDeadline.tv_sec) { //Congestion control timeout (120s) occurred
    			printf("[WARNING]: Congestion Control window timeout: no ack arrived in the last %u s \n", ackReceveingTimeoutInSeconds);
    			al_bundle_free(&bundleAck);
    			cleanExit();
    		}
     	} else if (result == AL_ERROR) {
     		printf("[ERROR]: Error on al_socket_receive\n");
     		al_bundle_free(&bundleAck);
            cleanExit();
    	}	
    }//end while: it may have either received a valid ACK or the ack timer has fired or the client has forcefully terminated

    i1=isCtrlcReceived();
    i2=terminationThresholdReached();
    al_utilities_debug_print(DEBUG_L2,"Is ctrl+c received=%d, is termination threshold reached =%d \n", i1,i2);
    if (i2) printCongestionWindow();
}

void* _mainCongestionControlWindow(void* parm) {
	al_bundle_create(&bundleAck);
	while ( isSenderRunning() || ((getNumBundleInFlight() > 0) && !isCtrlcReceived() ) ) {
//client running false should result into an immediate exit
//Otherwise go on until the sender is running or the number of bundle in flight is >o
    	waitForNextCC_Window();
    	congestionControlSemPost();
    }
	al_bundle_free(&bundleAck);
    return NULL;
}


/**
 * \brief Interface to congestion window used by Sender to append a new bundle in 
 * flight (just sent) in the congestion window
 * 
 * \par Function name:
 *      addBundleInFlight
 * 
 * \par Date Written
 *      21/04/2021
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  21/04/21 | A. Zappacosta   |   Implementation 
*/
void addBundleInFlight(al_types_endpoint_id destinationBundleSent, al_types_creation_timestamp creationTsBundleSent){
    pushBundleInFlight(&headOfWindowList, destinationBundleSent, creationTsBundleSent);
}

void passRegistrationDescriptorToCongestionControlWindow(al_socket_registration_descriptor parameter){
    registration_descriptor = parameter;
}
