/* \file CongestionControlWindow.c
 *
 *  \brief  This file contains the functions to manage the congestion control window list.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of the present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <string.h>

#include "CongestionWindow.h"
#include <unified_api.h>
//static int secsMarginIbr = 2; //This to cope with a bug in IBR-DTN, where the timestamp in the bundle is changed after generation
static int numBundleInFlight = 0;

/**
 * \brief Given a reference to the head of the linked list and a bundle identifier 
 * (source, creationTs), append a new bundle in flight on the front of the list
 * 
 * \par Function name:
 *      pushBundleInFlight
 * 
 * \par Date Written
 *      20/04/2021
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  20/04/21 | A. Zappacosta   |   Implementation 
*/
void pushBundleInFlight(struct BundleInFlight** headOfWindowList, al_types_endpoint_id destinationBundleSent, al_types_creation_timestamp creationTsBundleSent){
    struct BundleInFlight* newBundleInFlight = (struct BundleInFlight*)malloc(sizeof(struct BundleInFlight));
    newBundleInFlight->source = destinationBundleSent;
    newBundleInFlight->creation_ts = creationTsBundleSent;
    newBundleInFlight->nextBundleInFlight = (*headOfWindowList);
    (*headOfWindowList) = newBundleInFlight;
    numBundleInFlight++;
}

int equalsBundleID (struct BundleInFlight* ACK, al_types_endpoint_id destinationBundleSent, al_types_creation_timestamp creationTsBundleSent) {
//TODO The margin for IBRDTN has been deliberately removed to avoid problems in  Unibo-BP. A new fix should be in order.
	return ((ACK->creation_ts.time == creationTsBundleSent.time)
            && ACK->creation_ts.seqno == creationTsBundleSent.seqno
            && !strcmp(ACK->source.uri, destinationBundleSent.uri));
}

/**
 * \brief Given a reference to the head of the linked list and a bundle identifier 
 * (source, creationTs), deletes the first occurence of this budle in the list
 * 
 * \par Function name:
 *      deleteBundleInFlight
 * 
 * \par Date Written
 *      20/04/2021
 *
 * \return DeletionResult
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  20/04/21 | A. Zappacosta   |   Implementation 
*/
DeletionResult deleteBundleInFlight(struct BundleInFlight** headOfWindowList, al_types_endpoint_id destinationBundleSent, al_types_creation_timestamp creationTsBundleSent){
    struct BundleInFlight *temp = *headOfWindowList, *prev = NULL;
    
    // If the head node itself holds the bundle to be deleted
    if (temp != NULL && equalsBundleID(temp, destinationBundleSent, creationTsBundleSent)){

        *headOfWindowList = temp->nextBundleInFlight; // Changed head
        free(temp); // free old head

        numBundleInFlight--;
        return SUCCESSFULL_DELETION;
    }
 
    // Look for the bundle to be deleted, keep track of the
    // previous node as we need to change 'prev->next'
    while (temp != NULL && !equalsBundleID(temp, destinationBundleSent, creationTsBundleSent)) {

        prev = temp;
        temp = temp->nextBundleInFlight;

    }
 
    // If the bundle is not present in the linked list
    if (temp == NULL)
        return UNSUCCESSFULL_DELETION;
 
    // Otherwise unlink the found node from the linked list
    prev->nextBundleInFlight = temp->nextBundleInFlight;

    numBundleInFlight--;
 
    free(temp);
    return SUCCESSFULL_DELETION;
}

/**
 * \brief Print the content of the congestion window linked list starting from a
 * given node. The output is similar to the following:
 * 
 * Congestion window:
 * ipn:3.2000 ; 672387780 ; 1
 * ipn:3.2000 ; 672387781 ; 1
 * ...
 * ipn:3.2000 ; 672387792 ; 3
 * 
 * \par Function name:
 *      printWindow
 * 
 * \par Date Written
 *      20/04/2021
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  20/04/21 | A. Zappacosta   |   Implementation 
*/
void printWindow(struct BundleInFlight* startingBundleInFlight){
    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Congestion window bundles in flight:\n");
    while (startingBundleInFlight != NULL) {
        al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] %s ; %lu ; %ld\n", startingBundleInFlight->source.uri, startingBundleInFlight->creation_ts.time, startingBundleInFlight->creation_ts.seqno);
        startingBundleInFlight = startingBundleInFlight->nextBundleInFlight;
    }
}

/**
 * \brief Returns the actual number of bundles in flight.
 * 
 * \par Function name:
 *      getNumBundleInFlight
 * 
 * \par Date Written
 *      21/03/2022
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  21/03/2022 | A. Zappacosta   |   Implementation 
*/
int getNumBundleInFlight(){
	return numBundleInFlight;
}
