/** \file ClientFileMode.c
 *
 *  \brief  This file contains the Client File Mode functions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/



#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <unified_api.h>

#include "ClientFileMode.h"

#include "../../ClientPayloadManager/ClientPayloadManager.h"
#include "../../../../dtnperfHeaders.h"

static long fileBytesToSend;

static int dtnperfHeaderLength;

void setFileMode(ClientMode* clientMode, char* absFileNameParameter, FileInfo* fileInfo){

	char* fileName;
	int fd;

	fd = open(absFileNameParameter, O_RDONLY);

	if(fd < 0){
		printf("[DTNperf syntax error] Unable to open file %s\n", absFileNameParameter);
		exit(0);
	} else {
		fileInfo->fileLength = lseek(fd, 0, SEEK_END);
		lseek(fd, 0, SEEK_SET);
		close(fd);
	}

	fileInfo->absName=absFileNameParameter;//it could have been set by the parser; left here

	fileName = malloc(strlen(fileInfo->absName) + 1);
	memcpy(fileName, absFileNameParameter, strlen(absFileNameParameter) + 1);

	//Extract relativeFileName from filename
	char *token = strtok(fileName, "/");
	char *relativeFileName = strdup(token);
	// loop through the string to extract all other tokens
	while( token != NULL ) {
		token = strtok(NULL, "/");
		if(token != NULL){
			free(relativeFileName);
			relativeFileName = strdup(token);
		}
	}
	fileInfo->nameLength=strlen(relativeFileName);
	dtnperfHeaderLength=MIN_DTNPERF_HEADER_SIZE+fileInfo->nameLength;
	//dtnperfHeaderLength is global to be passed to prepareNextBundleFileMode below in this file
	fileInfo->relName=relativeFileName;
//	setRelativeFileNameInClientPayloadManager(relativeFileName);


	//    char *fileName = strdup(absFileNameParameter);//CCaini Sembra piu' semplice...
	clientMode->type = ClientFileModeType;
	clientMode->isTerminated = isTerminated_clientFileMode; //polymorphism; it is a pointer to the function
	clientMode->prepareNextBundle     = prepareNextBundle_clientFileMode; //polymorphism; it is a pointer to the function
	fileBytesToSend = fileInfo->fileLength;//fileBytesToSend is the counter to be decremented
	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Transmitting real data in File mode; file= %s\n", absFileNameParameter);
}

bool isTerminated_clientFileMode(){
    return (fileBytesToSend <= 0);
}

void prepareNextBundle_clientFileMode(long bundlePayload, al_types_bundle_object *bundle) {
	long fileSegmentLength;
	fileSegmentLength=bundlePayload-dtnperfHeaderLength;//the standard Segment size
	fileBytesToSend = fileBytesToSend - fileSegmentLength;//decrement the counter to obtain the B to be sent yet
	if(fileBytesToSend>0) {	//There is still something to send
		if(fileBytesToSend < fileSegmentLength) {//not enough B left to fill a standard Segment
			fileSegmentLength=fileBytesToSend;//reduce the next segment (necessarily the last)
			bundlePayload=fileSegmentLength+dtnperfHeaderLength;//calculate the (reduced) bundlePayload
		}
		prepareBundlePayload(ClientFileModeType,bundlePayload,bundle);
	} else{
		fileBytesToSend = 0;//set to zero only for safety; it should never be negative; then do nothing
	}
}

