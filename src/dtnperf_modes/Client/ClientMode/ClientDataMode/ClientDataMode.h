/** \file ClientDataMode.h
 *
 *  \brief  This file contains the declaration of ClientDataMode.c functions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *	\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *	\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/
#ifndef CLIENTDATAMODE_H
#define CLIENTDATAMODE_H

//#include "../../Client.h"
#include "../ClientMode.h"


bool isTerminated_clientDataMode();
void prepareNextBundle_clientDataMode(long bundlePayload, al_types_bundle_object *bundle);
void setDataMode(ClientMode* clientMode);
void setDataModeDataToSend(ClientMode* clientMode, long quantity);

#endif
