/** \file ClientDataMode.c
 *
 *  \brief  This file contains the Client Data Mode functions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *	\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *	\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "ClientDataMode.h"

#include "../../ClientPayloadManager/ClientPayloadManager.h"

static long bytesToSend;

bool isTerminated_clientDataMode() {
  return (bytesToSend <= 0);
}

void prepareNextBundle_clientDataMode(long bundlePayload, al_types_bundle_object *bundle) {
	bytesToSend = bytesToSend - bundlePayload;//updates the amount of bytes to be sent
	if(bytesToSend >0 && bytesToSend < bundlePayload){//There are still bytes to be sent, but less than standard payload
    //the last bundle must have a reduced payload size! New size = bytesToSend
//		prepareLastBundlePayload(ClientDataModeType,bytesToSend,bundle);
		prepareBundlePayload(ClientDataModeType,bytesToSend,bundle);
	}
}

void setDataMode(ClientMode* clientMode) {
  clientMode->type      = ClientDataModeType;
  clientMode->isTerminated   = isTerminated_clientDataMode;//polymorphism; it is a pointer to the function
  clientMode->prepareNextBundle = prepareNextBundle_clientDataMode;//polymorphism; it is a pointer to the function
  al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Transmitting dummy data in Data mode \n");
}

void setDataModeDataToSend(ClientMode* clientMode, long dataToSend) {
  clientMode->dataMode.quantity = dataToSend;
  bytesToSend = dataToSend;
  al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Requested Transmission of %ld bytes of data \n", bytesToSend);
}
