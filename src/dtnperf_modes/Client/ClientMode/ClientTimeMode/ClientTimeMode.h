/** \file ClientTimeMode.h
 * 
 * \brief Declarations of the Time mode functions
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 * 
 * \author Antony Zappacosta, antony.zappacosta@studio.unibo.it
 * 
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 * 
 ***********************************************/

#ifndef CLIENTTIMEMODE_H
#define CLIENTTIMEMODE_H

#include <time.h>

#include "../ClientMode.h"



bool isTerminated_clientTimeMode();
void prepareNextBundle_clientTimeMode(long bundlePayload, al_types_bundle_object *bundle);
void setTimeModeTestDuration(ClientMode* clientMode, int seconds);
void setTimeMode(ClientMode* clientMode);

#endif
