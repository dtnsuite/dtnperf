/** \file ClientDataMode.c
 *
 *  \brief  This file contains the ClientDataMode functions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/


#include <stdio.h>
#include <stdlib.h>
#include "ClientTimeMode.h"

static time_t getCurrentTime();

static time_t stopTime;

bool isTerminated_clientTimeMode() {
    time_t current_time = getCurrentTime();
    return (current_time > stopTime);
}

void prepareNextBundle_clientTimeMode(long bundlePayload, al_types_bundle_object* bundle){
    //Do nothing. In Time mode all bundles are the same as the first
}

void setTimeModeTestDuration(ClientMode* clientMode, int seconds){
    clientMode->timeMode.seconds = seconds;
    stopTime = time(NULL) + seconds;
    al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Requested transmission interval %d (s)\n", clientMode->timeMode.seconds);
}

void setTimeMode(ClientMode* clientMode){
    clientMode->type       = ClientTimeModeType;
    // Set all function pointers
    clientMode->isTerminated   = isTerminated_clientTimeMode;
    clientMode->prepareNextBundle     = prepareNextBundle_clientTimeMode;
    al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Transmitting dummy data in Time mode");
}

static time_t getCurrentTime(){
    time_t current_time;
    current_time = time(NULL);

    if (current_time == ((time_t)-1))
    {
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
        exit(EXIT_FAILURE);
    }
    return current_time;
}
