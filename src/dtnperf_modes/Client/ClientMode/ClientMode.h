/* \file ClientMode.h
 *
 *  \brief  This file contains the declarations of structures used in ClientMode.h
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of the present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#ifndef CLIENTMODE_H
#define CLIENTMODE_H

#include <stdbool.h>

#include "unified_api.h"

typedef enum {
  ClientTimeModeType = 1,
  ClientDataModeType = 2,
  ClientFileModeType = 3
} ClientModeType;

typedef enum {
  KILOBYTES = 1000,
  MEGABYTES=1000*KILOBYTES,
} Units;

typedef struct {
  long     quantity;
} ClientDataMode;

typedef struct {
  int     seconds;
} ClientTimeMode;

/*metto, almeno per ora, le sole informazioni indipendenti; cioè quelle non ricavabili 
senza i dati qui contenuti*/
/*
typedef struct {
  int fileExpirationTime;
  char *fileName;
} ClientFileMode;
*/

typedef struct {
  char *absName;
  char *relName;
  int fileLength;
  int nameLength;
} FileInfo;

typedef struct {
  ClientModeType          type;
  bool               (*isTerminated)();
  void              (*prepareNextBundle)(long bytes, al_types_bundle_object *bundle);//aggiunte uguali a tutte
  
  union {
    ClientDataMode      dataMode;
    ClientTimeMode      timeMode;
  };
} ClientMode;

#endif
