/* \file ClientPayloadManager.c
 *
 *  \brief  This file contains the functions of the Client Payload Manager.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of the present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/
#include "ClientPayloadManager.h"

#include "../../../dtnperf_system_libraries.h"
#include "../../../dtnperf_definitions.h"
#include "../../../dtnperfHeaders.h"


static dtnperfHeader_t clientHeader;
static bundle_options_t bundle_options; //passed by runClient in Client.c

static int bundlePayload;
int dtnperfHeaderLength;

static FileInfo fileInfo;

void copyFileSegmentToBundlePayload(int fileSegmentOffset, int dtnperfPayloadLength, void** cursor){
    int fd = open(fileInfo.absName, O_RDONLY);//
    lseek(fd, fileSegmentOffset, SEEK_SET);//
    int bytesToReadFromFile = dtnperfPayloadLength;
    if(bytesToReadFromFile > fileInfo.fileLength) bytesToReadFromFile = fileInfo.fileLength;

    if((read(fd, cursor, bytesToReadFromFile)) < 0){ //copy into buffer the file (segment)
        printf("[DTNperf syntax error] Unable to open file %s\n", fileInfo.absName);
        exit(0);
    }
    close(fd);//
}

void buildDtnperfClientHeaderStructure(ClientModeType clientModeType, CongestionControlType congestionControlType){
//    int fileNameLength=0;
//    fileNameLength=fileInfo.nameLenght;
	if(clientModeType == ClientTimeModeType || clientModeType == ClientDataModeType){
		dtnperfHeaderLength = MIN_DTNPERF_HEADER_SIZE;
	}
	else if (clientModeType == ClientFileModeType) {
		dtnperfHeaderLength = MIN_DTNPERF_HEADER_SIZE + fileInfo.nameLength;
	}
	// Set the dtnperfHeader structure
	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Set the dtnperfHeader structure for the data bundles \n");

	clientHeader.version = DTNPERF_HEADER_VERSION;

	if(clientModeType == ClientTimeModeType || clientModeType == ClientDataModeType) {
		clientHeader.type = 0;
	}
	else if(clientModeType == ClientFileModeType){
		clientHeader.type = 1;
	}

	clientHeader.ack_lifetime = bundle_options.lifetime;

	if(congestionControlType == CongestionControlWindowType) clientHeader.ack_QoS.flag = 1;
	else if(congestionControlType == CongestionControlRateType) clientHeader.ack_QoS.flag = 0;
	clientHeader.ack_QoS.priority=bundle_options.cardinal;

//additional fields for File header
	if(clientModeType == ClientFileModeType) {


	clientHeader.file_name_length = fileInfo.nameLength;
	clientHeader.file_exp = clientHeader.ack_lifetime;//Maybe different in future versions.
	clientHeader.file_size =fileInfo.fileLength;
	clientHeader.file_name = fileInfo.relName;
	clientHeader.segment_offset = 0;//it will be modified
	}
}

void prepareBundlePayload (ClientModeType clientModeType, int bundlePayloadDimension, al_types_bundle_object* bundle){
	void* buffer;
	void* cursor;
	int bufferLength;
	static int firstTime=1;
	bundlePayloadDimension=MAX(bundlePayloadDimension,dtnperfHeaderLength);//it can be changed only at the last bundle
	bundlePayload = bundlePayloadDimension;//bundlePayload is global in this file.

//Create or reuse the buffer
	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Serialize the DTNperf header...\n");
	if (firstTime){
		buffer=malloc(bundlePayload);
		firstTime=0;
	}
	else {
		buffer=bundle->payload->buf.buf_val;
	}
    bufferLength=serializeDtnperfHeader(clientHeader, buffer);//bufferLength in output

    cursor=buffer+bufferLength;
	if (clientModeType == ClientTimeModeType || clientModeType == ClientDataModeType) {	//Data/Time mode
		al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Filling DTNperf payload with dummy data...\n");
		memset(cursor, 'x', bundlePayload-bufferLength);// fill the dtnperfPayload with "X"
	} else{
//		setFileModeOverhead(dtnperfHeaderLength);//copia la variabile dtnperfHeaderLenght in fileModeOverhead (globale)
		al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Filling DTNperf payload  with one file segment...\n");
		copyFileSegmentToBundlePayload(clientHeader.segment_offset, bundlePayload-bufferLength, cursor);//copio su buffer un segmento a partire dall'offset
		clientHeader.segment_offset += (bundlePayload - dtnperfHeaderLength);//increment the offset for next time
	}
    al_bundle_set_payload_mem(bundle, buffer, bundlePayload);//put the buffer in the bundle payload
//    free(buffer);
	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Payload prepared\n");
}

void prepareStopBundlePayload (al_types_bundle_object* bundle, int sent_bundles){
	void* buffer;
	int bufferLength;
    buffer=bundle->payload->buf.buf_val;
	clientHeader.type = 3; //STOP
	clientHeader.sent_bundles=sent_bundles;
	clientHeader.lifetime=bundle_options.lifetime;
	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Serialize the DTNperf STOP bundle...\n");
	bufferLength=serializeDtnperfHeader(clientHeader, buffer);//bufferLength in output
	al_bundle_set_payload_mem(bundle, buffer, bufferLength);//put the buffer in the bundle payload
	al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Payload prepared\n");
}


void passBundleOptionsToClientPayloadManager(bundle_options_t parameter){
    bundle_options = parameter;
}

void passFileInfoToClientPayloadManager(FileInfo parameter){
    fileInfo=parameter;
}


