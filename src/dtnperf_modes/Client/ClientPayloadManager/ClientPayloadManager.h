/* \file ClientPayloadManager.h
 *
 *  \brief  This file contains the declarations of functions in ClientPayloadManager.c
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 *\authors of the present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#ifndef CLIENTPAYLOADMANAGER_H
#define CLIENTPAYLOADMANAGER_H

#include "unified_api.h"
#include "../CongestionControl/CongestionControl.h"
#include "../ClientMode/ClientMode.h"


void buildDtnperfClientHeaderStructure(ClientModeType clientModeType, CongestionControlType congestionControlType);
void prepareStopBundlePayload (al_types_bundle_object* bundle, int sent_bundles);
void prepareBundlePayload(ClientModeType clientModeType,
                    int bundlePayloadDimension,
					al_types_bundle_object* bundle
                    );

void copyFileSegmentToBundlePayload(int fileSegmentOffset, int dtnperfPayloadLength, void** cursor);

void setClientPayload();
void setDataTimeHeader();

void passBundleOptionsToClientPayloadManager(bundle_options_t parameter);
void passFileInfoToClientPayloadManager(FileInfo parameter);

#endif
