/** \file Sender.c
 *
 *  \brief  This file contains the Sender thread functions of the DTNperf v4 Client. The Sender aim is to send bundles whjen allowed by the Congestion Control thread
 *
 *  \details The main function is "_mainSender", which consists of a while loop.
 *  	The Client normally terminates when one of the following 3 condition is met:
 *  	1) in Time mode, after the wanted bundle generation time has elapsed
 *  	2) In Data mode, after the wanted amount of data has been sent
 *  	3) in File mode, after the File has been passed to BP (one segment per bundle)
 *  	The 3 congestion controls and 3 conditions are tackled by taking advantage of homomorphism.
 *  	If the monitor option is set, a STOP bundle is sent to the monitor to let it know how many bundles have been sent
 *  	and thus how many delivered BSR it should be waiting for before terminating the "Session" corresponding to that
 *  	Client instance.
 *  	The Client termination can also be forced by pressing Ctrl+c. This signal is managed by a handler (ctrlcHandler).
 *
 *  	For exhaustive information, the reader is referred to DTNperf_4 documentation.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Sender.h"
#include "../Client.h"
#include "../CongestionControl/CongestionControlWindow/CongestionControlWindow.h"
#include "../ClientPayloadManager/ClientPayloadManager.h"
#include <unified_api.h>
#include "../../../dtnperf_definitions.h"


static pthread_t senderThread;
static al_types_bundle_object bundle;
static al_types_endpoint_id destinationEid;
static al_types_endpoint_id reportToEid;
static al_socket_registration_descriptor registration_descriptor;

//aggiunte da me provvisoriamente
static bundle_options_t bundleOptions;
static ClientModeType clientModeType;
static CongestionControlType congestionControlType;
static int bundlePayload;

/**
 * \brief Main function of Sender thread. It sends bundles until the termination condition is met:
 * transmission time elapsed (in Time mode),
 * data quantity reached (in Data mode),
 * file transmission completed (in File mode).
 * Termination can be anticipated by CTRL+C
 * 
 * \par Function name:
 *      _mainSender
 * 
 * \par Date Written:
 *      07/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  07/11/20 | A. Zappacosta   |   Implementation
 *  21/04/21 | A. Zappacosta   |   Add bundle to congestion window if ack is required
*/
static void* _mainSender(void* parm) {

	int sent_bundles=0;
	al_types_endpoint_id nullEid;
	al_error alError;

	al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Sender thread started\n");
	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Destination: %s  Report-to:%s\n", destinationEid.uri, reportToEid.uri);
    al_bundle_create(&bundle);
    al_bundle_options_set(&bundle, bundleOptions);
    prepareBundlePayload(clientModeType, bundlePayload, &bundle);

    while (!isCtrlcReceived() && !terminationThresholdReached()){//terminationThresholdReached uses polymorphism;
// to check if the termination condition of the sender thread has been reached; if yes skip the while
//    	printf("%s pre al_socket_send\n", __FUNCTION__);
    	alError=al_socket_send(registration_descriptor, bundle, destinationEid, reportToEid);
        if (alError!=AL_SUCCESS) {
        	sent_bundles += 1;
        	al_utilities_debug_print(DEBUG_L0,"[DEBUG_L0] Error in sending bundle num. %d\n", sent_bundles);
        	exit(1);
        }
    	al_utilities_debug_print(DEBUG_L0,"[DEBUG_L0] Bundle timestamp: %llu.%lu\n", (unsigned long long) bundle.spec->creation_ts.time, (unsigned long) bundle.spec->creation_ts.seqno);
        if(congestionControlType == CongestionControlWindowType){
            addBundleInFlight(bundle.spec->source, bundle.spec->creation_ts);
        }
        sent_bundles=incBundleSent(bundle.spec->source, bundle.spec->creation_ts);//used only by STOP bundle
        incTotalDataSent(getBundlePayloadDimension());//increment a counter in client.c that
 //            is used only by final report; it is NOT used by clientModes, which have their own counters
        prepareNextBundle(&bundle);
 /*		it uses polymorphism to call 3 different functions, depending on the data mode
        clientMode.bundleSent is a pointer to the actual function set at client Mode start up:

        bundleSent_clientTimeMode: do nothing, all bundles are the same as the first (payload is dummy)
        bundleSent_clientDataMode: increase counter data sent; reduce the bundlePaylod if the last bundle is shorter, otherwise let it unaltered (payload is dummy).
        bundleSent_clientFileMode: decrease the counter of (file) data to be sent; it always need to prepare a new bundlePayload; if the last bundle needs
        a shorter segment length, the bundlePayload is reduced accordingly
 */
        waitToSend();//semaphore; go on if and when allowed by the congestion control thread
    }

	strcpy(nullEid.uri,"dtn:none");
	if (strcmp(reportToEid.uri,"dtn:none")){//If reportTo is different from "dtn:none" send the Stop bundle
		prepareStopBundlePayload (&bundle,sent_bundles);
		al_socket_send(registration_descriptor, bundle, reportToEid, nullEid);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] STOP bundle sent (passed to BP)\n");
	}
	al_bundle_free(&bundle);
    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Sender thread terminated\n");
    return NULL; //Sender thread terminated
}


/**
 * \brief Start the Sender thread using pthread_create and indicating _mainSender
 * as the start routine for this process
 * 
 * \par Function name:
 *      startSender
 * 
 * \par Date Written:
 *      07/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  07/11/20 | A. Zappacosta   |   Implementation
*/
void startSender(){
    pthread_create(&senderThread, NULL, _mainSender, NULL);
}

/**
 * \brief The caller put itself in a blocking wait until the Sender thread is 
 *  terminated
 * 
 * \par Function name:
 *      waitForStopSender
 * 
 * \par Date Written:
 *      07/11/20
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  07/11/20 | A. Zappacosta   |   Implementation 
*/
void waitForStopSender(){
    pthread_join(senderThread, NULL);
}

/**
 * \brief Returns the size of the last sent bundle
 * 
 * \par Function name:
 *      getBundleDimension
 * 
 * \par Date Written:
 *      06/11/20
 *
 * \return unsigned int
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  06/11/20 | A. Zappacosta   |   Implementation 
*/
unsigned int getBundlePayloadDimension(){ //CCaini si puo' sostituire con una variabile
    unsigned int result;
    al_bundle_get_payload_size(bundle, &result);
    return result;
}

/**
 * \brief Set the registration descriptor
 * 
 * \par Function name:
 *      setRegistrationDescriptor
 * 
 * \par Date Written:
 *      25/10/21
 *
 * \return void
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  25/10/21 | A. Zappacosta   |   Implementation
*/
void passRegistrationDescriptorToSender(al_socket_registration_descriptor parameter){
    registration_descriptor = parameter;
}

/**
 * \brief Returns the registration descriptor
 * 
 * \par Function name:
 *      getRegistrationDescriptor
 * 
 * \par Date Written:
 *      06/11/20
 *
 * \return al_socket_registration_descriptor
 *
 *
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         |   DESCRIPTION
 *  -------- | --------------- | -----------------------------------------------
 *  06/11/20 | A. Zappacosta   |   Implementation
*/
al_socket_registration_descriptor getRegistrationDescriptor(){
    return registration_descriptor;
}


void passDestinationToSender(al_types_endpoint_id parameter){
    destinationEid = parameter;
}

void passReporttoToSender(al_types_endpoint_id parameter){
    reportToEid = parameter;
}

void passBundleOptionsToSender(bundle_options_t parameter){
    bundleOptions = parameter;
}



void passCongestionControlTypeToSender(CongestionControlType parameter){
	congestionControlType = parameter;
}

void passClientModeTypeToSender (ClientModeType parameter){
	clientModeType = parameter;
}

void passBundlePayloadToSender(int parameter){
    bundlePayload = parameter;
}
