/** \file Sender.h
 *
 *  \brief  This file contains the prototypes of functions defined in Sender.c.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#ifndef SENDER_H
#define SENDER_H

#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

#include "unified_api.h"

#include "../CongestionControl/CongestionControl.h"

#include "../ClientMode/ClientMode.h"
//#include "../Client.h"

void startSender();
void waitForStopSender();

//functions to allow components in other files to get global variables defined in Sender.c
unsigned int getBundlePayloadDimension();
al_socket_registration_descriptor getRegistrationDescriptor();

//functions to pass global variables defined in Sender.c to global variables defined in other files
void passRegistrationDescriptorToSender (al_socket_registration_descriptor parameter);
void passDestinationToSender(al_types_endpoint_id destinationParameter);
void passReporttoToSender(al_types_endpoint_id monitorParameter);
void passBundleOptionsToSender(bundle_options_t bundle_options);
void passBundlePayloadToSender(int bundlePayloadDimension);
void passCongestionControlTypeToSender(CongestionControlType parameter);
void passClientModeTypeToSender (ClientModeType parameter);

#endif
