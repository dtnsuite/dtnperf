/** \file Client.c
 *
 *  \brief  This file contains the main Client functions of DTNperf v4 (including runClient and clientParsing).
 *  	The Client aim is to send data bundle to the Server. The data bundle payloads can either be dummy, or segments of a file.
 *  	The Client can also be used alone (in Congestion Control "Rate" only) with other receiving applications (especially if registered as ipn).
 *
 *
 *  \details The Client main function is runClient, which starts the Sender and the Congestion Control threads, whose code is in the
 *  	homonymous directories. Before, the clientParsing function is called, to parse the Client options, which are many. Note that bundle options have
 *  	already been parsed by the al_bundle_options_parse in the dtnperf main.
 *  	There are three sending modes: Time, Data and File. In Time and Data mode bundles of the wanted dimension are sent to the Server with
 *  	a dummy payload. In File mode a File is transferred to the Server in one or more bundles. The Segment files, one for each bundle, are collected by the Server and reassembled in the original file.
 *  	Out of order and duplications are tackled, but not possible bundle losses.
 *  	Bundles can be sent in either "Window" or "Rate" congestion control. In the former mode, bundles must be acked by the Server.
 *  	If the "monitor" option is added, BSR delivered are automatically requested, with report-to set to the Monitor EID.
 *  	Other BSR requested can be added by the user.
 *  	The Client normally terminates when one of the following 3 condition is met:
 *  	1) in Time mode, after the wanted bundle generation time has elapsed
 *  	2) In Data mode, after the wanted amount of data has been sent
 *  	3) in File mode, after the File has been passed to BP (one segment per bundle)
 *  	The 3 congestion controls and the 3 conditions are tackled by taking advantage of homomorphism.
 *  	The Client termination can also be forced by pressing Ctrl+c. This signal is managed by a handler (ctrlcHandler).
 *
 *  	For exhaustive information, the reader is referred to DTNperf_4 documentation.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Antony Zappacosta, antony.zappacosta@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include "Client.h"

#include "ClientMode/ClientTimeMode/ClientTimeMode.h"
#include "ClientMode/ClientDataMode/ClientDataMode.h"
#include "ClientMode/ClientFileMode/ClientFileMode.h"

#include "ClientPayloadManager/ClientPayloadManager.h"

#include "CongestionControl/CongestionControlRate/CongestionControlRate.h"
#include "CongestionControl/CongestionControlWindow/CongestionControlWindow.h"
#include "CongestionControl/CongestionControlWindow/CongestionWindow.h"

#include "Sender/Sender.h"

#include "../../dtnperf_definitions.h"
#include "../../dtnperfHeaders.h"

//GOODPUT PARAMETER COMPUTATION
static int totalBundleSent = 0;
static double totalDataSent = 0;

static int ctrlcReceived = 0, senderRunning=1;
static int fileModeOverhead = 0; //size of Client Header in File Modality. Dinamically computed
static CongestionControl congestionControl;


//The following 3 are set by the parser

static ClientModeType clientModeType; // ClientModeType index (Time|Data|File)
static ClientMode clientMode; //The polimorphic structure used by Client(Time|Data|File)Mode files
static int bundlePayload;

//Prototypes
void printClientHelp(boolean_t al_bundle_options_help);
uint64_t current_timestamp();

//Functions

int isCtrlcReceived(){
    return ctrlcReceived;
}

int isSenderRunning(){
    return senderRunning;
}

void ctrlcHandler(int signal){
    al_utilities_debug_print(DEBUG_L0,"[DEBUG_L0] Forced exit, please wait a little...\n");
    ctrlcReceived = 1; //This should force the sender to exit the sending while at next condition check
    unlockWaitToSend();//It unlocks the sender if locked by the wait to send
    waitForStopSender();
    senderRunning = 0;
    al_utilities_debug_print(DEBUG_L0,"[DEBUG_L0] Sender terminated wait a little...\n");
    waitForStopCongestionControl();
    al_utilities_debug_print(DEBUG_L0,"[DEBUG_L0] Comgestion Control terminated wait a little...\n");
    al_socket_unregister(1);
    al_socket_destroy();
    al_utilities_debug_print_debugger_destroy();
    exit(1);
}

void print_results(uint64_t startTime, uint64_t stopTime){

	double Goodput = 0;
	double Throughput = 0;
	uint64_t totalExecutionTimeMillis;
	double totalExecutionTimeSeconds, dataInMB, dataInKB;

    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] getting total execution time...");
    totalExecutionTimeMillis = stopTime - startTime;
    totalExecutionTimeSeconds = totalExecutionTimeMillis / 1000.0;
    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] total execution time = %.3f s\n", totalExecutionTimeSeconds);

    //print data

    if((totalDataSent / 1000000) > 1){ //print in MByte
        dataInMB = totalDataSent / 1000000.0;
        printf("\nBundles sent = %d, total data sent = %.3f Mbyte\n", totalBundleSent, dataInMB);
    } else if((totalDataSent / 1000) > 1){ // printf in kbyte
        dataInKB = totalDataSent / 1000.0;
        printf("\nBundles sent = %d, total data sent = %.3f kbyte\n", totalBundleSent, dataInKB);
    } else //print in byte
        printf("\nBundles sent = %d, total data sent = %f byte\n", totalBundleSent, totalDataSent);

    //print time

    printf("Total execution time %.3f seconds\n", totalExecutionTimeSeconds);

    //print Goodput || Throughput

    if(congestionControl.type == CongestionControlWindowType){
        Goodput = ((double)totalDataSent * 8) / ((double)totalExecutionTimeSeconds);
        if((Goodput / 1000000) > 1) printf("Goodput = %.3f Mbit/s\n", (Goodput / 1000000.0));
        else if((Goodput / 1000) > 1) printf("Goodput = %.3f kbit/s\n", (Goodput / 1000.0));
        else printf("Goodput = %.3f bit/s\n", Goodput);
    } else if(congestionControl.type == CongestionControlRateType){
        Throughput = ((double)totalDataSent * 8) / ((double)totalExecutionTimeSeconds);
        if((Throughput / 1000000) > 1) printf("Throughput = %.3f Mbit/s\n", (Throughput / 1000000.0));
        else if((Throughput / 1000) > 1) printf("Throughput = %.3f kbit/s\n", (Throughput / 1000.0));
        else printf("Throughput = %.3f bit/s\n", Throughput);
    }
    return;
}

//CCaini To be moved in the Unified API
int checkValidEID(al_types_endpoint_id endpoint_id, eid_scheme_t *eidScheme){
//It works but it is very hugly...
    int i,dot = 0, number = 0, result = 0;
    if(!strncmp(endpoint_id.uri, "ipn:", 4)){//check validity of ipn
        for(i = 4; i < strlen(endpoint_id.uri); i++){
            if(endpoint_id.uri[i] != '.' && !isdigit(endpoint_id.uri[i])) return 0;
            if(number == 0 && dot == 0 && isdigit(endpoint_id.uri[i])) number = 1;
            if(number == 1 && dot == 0 && endpoint_id.uri[i] == '.') dot = 1;
            if(number == 1 && dot == 1 && isdigit(endpoint_id.uri[i])) result = 1;
            *eidScheme = IPN_scheme;
        }
    } else if(!strncmp(endpoint_id.uri, "dtn:", 4)){  //for the dtn scheme, "dtn:" is the only control that is made
        result = 1;
        *eidScheme = DTN_scheme;
    }
    return result;
}

dtnperf_error_t clientParsing (application_options_t unparsed_options, bundle_options_t* bundle_options){

	char suffix, *fileName=NULL;
//	fileName = malloc(sizeof(char));
//	strcpy(fileName, "");
	al_types_endpoint_id destinationEid, reportToEid;
	strcpy(reportToEid.uri, "dtn:none");//default is null
	CongestionControlType congestionControlType=CongestionControlRateType;

	int optionIndex = 0, destinationFlag = 0, congestionControlFlag = 0, modeFlag = 0;
	long dataToSend=0;
	eid_scheme_t eidScheme;
	//default values
	int windowSize=0;

	double rate=0;
	RateType rateType=1;

	int testDuration=0;
	int payloadSize = 50000;//default value
	FileInfo fileInfo;

#define SKIP 1
#define HELP 2

	static struct option long_options[] =
	{
			{"client", no_argument, 0, SKIP},
			{"help", no_argument, 0, HELP },
			{"destination", required_argument, 0, 'd'},
			{"time", required_argument, 0, 'T'},
			{"data", required_argument, 0, 'D'},
			{"file", required_argument, 0, 'F'},
			{"window", required_argument, 0, 'W'},
			{"rate", required_argument, 0, 'R'},
			{"payload", required_argument, 0, 'P'},
			{"monitor", required_argument, 0, 'm'},
			{0, 0, 0, 0}	// The last element of the array has to be filled with zeros.
	};

	optind = 1;
	while((optionIndex = getopt_long(unparsed_options.app_opts_size, unparsed_options.app_opts, ":d:T:D:F:W:R:P:m:", long_options, NULL)) != -1){
		switch(optionIndex){
		case HELP:
			printClientHelp(TRUE);
			return ERROR;
			break;

		case SKIP:
			//skip --client
			break;

		case 'd':
			destinationFlag = 1;
			strcpy(destinationEid.uri, optarg);
			if(!checkValidEID(destinationEid,&eidScheme)){//It also finds the scheme
				printf("[DTNperf fatal error] in parsing BP EID: invalid eid string %s\n", destinationEid.uri);
				return ERROR;
			}
			if(eidScheme == DTN_scheme){
				strcat(destinationEid.uri, DTN_SCHEME_SEPARATOR); //append the dtn separator
			    strcat(destinationEid.uri, SERVER_DEMUX); //append the dtn demux token
			}
			break;

		case 'm':
			strcpy(reportToEid.uri, optarg);
			if(!checkValidEID(reportToEid, &eidScheme)){//It also find the scheme
				printf("[DTNperf fatal error] in parsing BP EID: invalid eid string %s\n", reportToEid.uri);
				return ERROR;
			}
			if(eidScheme == DTN_scheme){
				strcat(reportToEid.uri, DTN_SCHEME_SEPARATOR); //append the dtn separator
				strcat(reportToEid.uri, MONITOR_DEMUX);//append the dtn demux token
			}
			break;

		case 'T'://clientMode is global
			if (modeFlag==1) {//if modality flag was already set, print help: it cannot be set twice
				printClientHelp(FALSE);
				return ERROR;
			}
			modeFlag=1;
			clientModeType=ClientTimeModeType;
			testDuration=atoi(optarg);
			break;

		case 'D'://clientMode is global
			if (modeFlag==1) {//if modality flag was already set, print help: it cannot be set twice
				printClientHelp(FALSE);
				return ERROR;
			}
			modeFlag=1;
			clientModeType=ClientDataModeType;
			dataToSend = atol (optarg); //extract the number from the optarg string
			suffix = optarg[(strlen(optarg) - 1)]; //extract the suffix unit (B|k|M)
			switch (suffix)
			{
			case 'B':
				break;
			case 'k':
				dataToSend=dataToSend*1000;
				break;
			case 'M':
				dataToSend=dataToSend*1000000;
				break;
			default:
				printf("Wrong data (D) suffix!");
				printClientHelp(FALSE);
				return ERROR;
				break;
			}
			if (dataToSend<MIN_DTNPERF_HEADER_SIZE){
				printf("Dtnperf error: Data must be >%d \n", MIN_DTNPERF_HEADER_SIZE);
				printClientHelp(FALSE);
				return ERROR;
			}
			break;

		case 'F'://clientMode is global
			if (modeFlag==1) {//if modality flag was already set, print help: it cannot be set twice
				printClientHelp(FALSE);
				return ERROR;
			}
			modeFlag=1;
			clientModeType=ClientFileModeType;
			fileName = strdup(optarg);
			break;

		case 'W':
			if (congestionControlFlag==1) {//if congestion control flag was already set, print help: it cannot be set twice
				printClientHelp(FALSE);
				return ERROR;
			}
			congestionControlFlag=1;
			congestionControlType=CongestionControlWindowType;
			windowSize = atoi(optarg);
			break;
		case 'R':
			if (congestionControlFlag==1) {//if congestion control flag was already set, print help: it cannot be set twice
					printClientHelp(FALSE);
					return ERROR;
			}
			congestionControlFlag=1;
			congestionControlType=CongestionControlRateType;
			rate = atof(optarg);
			suffix = optarg[(strlen(optarg) - 1)];
			switch (suffix)
			{
			case 'k'://value is in kbit/s
				rateType=BitPerSecond;
				rate=rate*1000;
				break;
			case 'M':
				rateType=BitPerSecond;
				rate=rate*1000000;
				break;
			case 'b':
				rateType=BundlePerSecond;
				break;
			default:
				printf("Wrong rate (R) suffix!");
				printClientHelp(FALSE);
				return ERROR;
				break;
			}
			break;

			case 'P':
				payloadSize = atoi(optarg);
				suffix = optarg[(strlen(optarg) - 1)];
				switch (suffix)
				{
				case 'B':
					break;
				case 'k':
					payloadSize = payloadSize * 1000;
					break;
				case 'M':
					payloadSize = payloadSize * 1000000;
					break;
				//...
				default:
					printf("Wrong payload (P) suffix!");
					printClientHelp(FALSE);
					return ERROR;
				break;
				}
				break;

			case '?':
				printf("Dtnperf client parser error, option not recognized: %s\n", unparsed_options.app_opts[optind-1]);
				printClientHelp(TRUE);
				return ERROR;
				break;
			default:
				break;
		}
	}


//Check compulsory parameters
	if(!destinationFlag || !modeFlag || !congestionControlFlag){//destination, mode and congestion control must be set
		printClientHelp(FALSE);
		return ERROR;
	}

//Payload sanity check
		if ((clientModeType==ClientTimeModeType|| clientModeType==ClientDataModeType) && payloadSize< MIN_DTNPERF_HEADER_SIZE){
					printf("Dtnperf error: Payload must be >=%d \n", MIN_DTNPERF_HEADER_SIZE);
					printClientHelp(FALSE);
					return ERROR;
				}
		if (clientModeType==ClientDataModeType && payloadSize>dataToSend){
				printf("Dtnperf error: Payload must be <=Data \n");
				printClientHelp(FALSE);
				return ERROR;
			}
		if (clientModeType==ClientFileModeType && payloadSize<500){
			printf("Dtnperf error: Initial payload must be set >=500 in File mode \n");
			printClientHelp(FALSE);
			return ERROR;
		}


//Set the client mode
	if (clientModeType==ClientDataModeType){
		setDataMode(&clientMode);//populate the polymorphic clientMode structure (version for Data) in ClientDataMode.c
		setDataModeDataToSend(&clientMode, dataToSend);//add to clientMode the amount of B to be sent and set bytesToSend global in ClientDataMode.c
	}
	else if (clientModeType==ClientTimeModeType){
		setTimeMode(&clientMode);//populate the polymorphic clientMode structure (version for Time)in ClientTimeMode.c
		setTimeModeTestDuration(&clientMode, testDuration);//add to clientMode the wanted duration of client session
//Perche' due set?
	}
	else if (clientModeType==ClientFileModeType){

		setFileMode(&clientMode, fileName, &fileInfo);//populate the polymorphic clientMode structure (version for File) in ClientDataMode.c
//reduce payloadsize if greater than (the dtnperf header+file length)
//This check is here as it required fileInfo obtained in the instruction above
		payloadSize=MIN(payloadSize,fileInfo.fileLength+MIN_DTNPERF_HEADER_SIZE+fileInfo.nameLength);//it is necessary to reduce the payloadsize
	}

//Set the Congestion Control
	if (congestionControlType==CongestionControlWindowType){
		setCongestionControlWindow(&congestionControl, windowSize);
	}
	else if (congestionControlType==CongestionControlRateType){
		setCongestionControlRate(&congestionControl, rate, rateType, payloadSize);
	}

//Override BSR flags if the monitor EID is dtn:none (otherwise BSR are sent to the source by DTN2)
	bundle_options->delivery_reports = TRUE;
	if (!strcmp(reportToEid.uri, "dtn:none")){ //Temporary fix to a DTN2/DTNME bug
		bundle_options->reception_reports=0;
		bundle_options->custody_reports=0;
		bundle_options->deletion_reports=0;
		bundle_options->forwarding_reports=0;
		bundle_options->delivery_reports=0;
		bundle_options->irf_trace=0;
		bundle_options->ack_requested_by_application=0;
	}

	//Set the header Structure
	//buildDtnperfClientHeaderStructure requires a)the bundle options (lifetime and cardinal) and b)fileInfo
	//be already passed to ClientPayloadManager
	passBundleOptionsToClientPayloadManager(*bundle_options); // a)pass bundle_options to ClientPayloadManager
	passFileInfoToClientPayloadManager(fileInfo);// b)

	buildDtnperfClientHeaderStructure(clientModeType, congestionControlType);

//Pass a few parameters to global variables of Sender.c
	passDestinationToSender(destinationEid);//the destinationEid is passed to sender.c (where is global)
	passReporttoToSender (reportToEid);//the reportToEid is passed to sender.c (where is global)
	passBundlePayloadToSender(payloadSize);//to sender
    passCongestionControlTypeToSender(congestionControl.type);//to sender
    passClientModeTypeToSender (clientMode.type); //to sender

    passBundleOptionsToSender(*bundle_options); //pass bundle_options to sender; potrebbe essere portato fuori (togliendo  *)

    bundlePayload=payloadSize; //set bundlePayload (global),
    //La variabile globale bundlePayload serve alla prepareNextBundle. Non é bello.
    //Da notare che prepareBundlePayload può modificare il payload del bundle, ma solo se è l'ultimo.
    //per questo motivo non "copio" il nuovo valore "all'indietro", in "bundlePayload" di questo file.
    return SUCCESS;
}


int runClient(bundle_options_t bundle_options, dtn_suite_options_t dtn_suite_options, application_options_t unparsed_options){
	signal(SIGINT, ctrlcHandler);
	
	al_socket_registration_descriptor registration_descriptor;
	uint64_t startTime,stopTime;
	dtnperf_error_t error;
	al_error alError;
	
	char *client_demux_string;
	int pid = ((int) getpid());
	client_demux_string = malloc(
			strlen(CLIENT_DEMUX) + 3 + ((int) log10(pid)));
	sprintf(client_demux_string, "%s_%d", CLIENT_DEMUX, pid);
	int client_service_number=pid;
			//
	error=clientParsing (unparsed_options, &bundle_options);  //pointer as BSR bundle options are modified
    if (error) return (1); //esci
	
    alError=al_socket_register(&registration_descriptor, client_demux_string, client_service_number, dtn_suite_options.eid_format_forced, dtn_suite_options.ipn_local, dtn_suite_options.ip, dtn_suite_options.port);
	if (alError) return(1);

	al_utilities_debug_print(DEBUG_L2,
					"[DEBUG_L2] connection to local BP daemon: done\n");
	free(client_demux_string);

    passRegistrationDescriptorToCongestionControlWindow(registration_descriptor); //idem to CongestionControlWindow.c
    passRegistrationDescriptorToSender(registration_descriptor);//copy registration descriptor to sender.c global variable

    

    startTime = current_timestamp(); //in ms; used only by final report to compute the total execution time

    startSender(); //Start Sender thread

    startCongestionControl(&congestionControl); //Start Congestion control thread

    waitForStopSender(); //Wait until the Sender thread terminates

    senderRunning = 0;

//    stopCongestionControl();//It actually increments by one the semaphore to allow the sender thread cycle to exit from
//the blocking call "wait to send" so that the sender can terminates; I would say that is useful only with ctrl+c

    waitForStopCongestionControl(); //Wait until the congestion control thread terminates

    stopTime = current_timestamp(); //used only by final report to compute the total execution time
    /* Normal Termination
     * The Sender thread terminates first; the congestion control terminates immediately after if CC is rate based, otherwise if window based only after all bundles in flight have been acked
     *
     * Abnormal termination
     * 1) In window based if the waiting timer of ACKs expires (120s from the last ACK received)
     * 2) Ctrl+c
     */

    print_results(startTime,stopTime);

    al_socket_unregister(registration_descriptor);
    return 0;
}

int terminationThresholdReached(){
    return clientMode.isTerminated();//Polymorphism; it will call either
//    isTerminated_clientDataMode()
// or isTerminated_clientFileMode()
// or isTerminated_clientTimeMode()
}

//the boolean input value indicates if we want to print also the help of the al_bp parser layer
void printClientHelp(boolean_t al_bundle_options_help){
     printf("\nSyntax:\n");
     printf("--client -d <dest_eid> <[-T <s> | -D <num> | -F <filename>]> <[-W <size> | -R <rate>]> [all_options]\n\n");
	 printf("dtnperf client options:\n");
	 printf("-d, --destination <eid>     Destination EID (i.e. server EID).\n");
	 printf("-T, --time <seconds>        Time-mode: seconds of transmission.\n");
     printf("-D, --data <num[B|k|M]>     Data-mode: amount data to transmit; B = Byte, k = kByte, M = MByte. Default 'M' (MB). According to the SI and the IEEE standards 1 MB=10^6 bytes\n");
	 printf("-F, --file <filename>       File-mode: file to transfer\n");
	 printf("-W, --window <size>         Window-based congestion control: max number of bundles in flight). Not possible in UD3TN\n");
	 printf("-R, --rate <rate[k|M|b]>    Rate-based congestion control: Tx rate. k = kbit/s, M = Mbit/s, b = bundle/s. Default is kb/s\n");
	 printf("-P, --payload <size[B|k|M]> Bundle payload size; B = Byte, k = kByte, M = MByte. Default= 'k'. According to the SI and the IEEE standards 1 MB=10^6 bytes.\n");
	 printf("-m, --monitor <eid>         External monitor EID.\n"); 
	 printf("--help                      This help.\n");
	 if(al_bundle_options_help == TRUE){
		 printf("%s\n", al_bundle_options_get_dtnsuite_help());
		 printf("%s\n", al_bundle_options_get_bundle_help());
	 }
}

uint64_t current_timestamp() {
    struct timeval te; 
    uint64_t milliseconds;
    gettimeofday(&te, NULL); // get current time
    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Sampled actual time_sec = %d s\n", (u_int)te.tv_sec);
    milliseconds = te.tv_sec*1000 + te.tv_usec/1000; // calculate milliseconds
    return milliseconds;
}


int incBundleSent(al_types_endpoint_id source, al_types_creation_timestamp creationTs){
    totalBundleSent++;
    al_utilities_debug_print(DEBUG_L2,"[DEBUG_L2] Number of bundles sent:%d\n", totalBundleSent);
    return totalBundleSent;
}

void incTotalDataSent(long dataSent){
    totalDataSent += dataSent;
}

void setFileModeOverhead(int dtnperfHeader){
    fileModeOverhead = dtnperfHeader;
}

void cleanExit(){//Non mi sembra tanto clean...
    printf("Dtnperf client: cleanExit\n");
    exit(1);//dipende se chiamat da un errore o se terminazione normale.
    return;
}


void passBundlePayloadToClient(int parameter){
    bundlePayload= parameter;
}


void prepareNextBundle(al_types_bundle_object* bundle){//it uses polymorphism to call 3 different functions, depending on the data mode
    clientMode.prepareNextBundle(bundlePayload, bundle);//clientMode.bundleSent e' un puntatore alla funzione che ppoi viene chiamata dipendente da file time o data mode settata a inizio programma
}
//It is difficult to move because it requires clientMode, which is polimorphic

