/** \file Server.c
 *
 *  \brief  This file contains the main Server functions of DTNperf v4.
 *  	The Server aim is to receive bundle sent by the Client and, if sent in "Window" congestion control mode, to acknowledge them.
 *  	Bundle can curry either dummy data (File and Time modes) or segment(s) of file (File mode).
 *  	In the latter case, segments are saved in temporary files, until either: the whole file is received, a timer expires.
 *  	As for the Monitor, only one instance of the Server is allowed on the same machine. One Server can receive bundles from many
 *  	concurrent Client instances, running on the same or on different nodes.
 *
 *  \details The Server main function is the runServer; The Server consists of a main thread and an auxiliary thread.
 *  	The runnServer contains the main thread; its core (an infinite while) receives bundles (sent by the Client) and send back
 *  	an ACK bundle if requested by the client. If the received bundle contains a File segment, the segment is saved in a temporary hidden file of the
 *  	dtnperf_files directory under the user's home. To prevent a file sent by one node from being overwritten by a homonymous file sent by a
 *  	different node, nodes have different subdirectories under the root dtnperf_files. This does not prevent a file to be later overwritten
 *  	by a homonymous  file sent by the same node.
 *  	The temporary files are normally closed once all segments have been received. In this case the file becomes visible.
 *  	If the maximum assembling time has elapsed (this information item is carried by the DTNMperf header of received data bundles) before
 *  	receiving all segments, the temporary hidden file is closed and received segments cancelled.
 *  	This is the task of the auxiliary thread, the "file Expiration Timer". It is started inside the runServer; it periodically checks
 *  	if temporary files can be closed (all segments arrived), deleted (the assembling threshold has been reached), or just left unaltered.
 *  	As for the Monitor, only one instance of the Server is allowed on the same machine. To stop the Server you need to press ctrl+C.
 *  	This is intercepted by the "closing server handler" (active in runServer), which in turns calls the "server clean exit" function.
 *  	This sets to true  the forced_close variable, used by the auxiliary thread to terminate. As this variable is written by the main thread
 *  	it is protected by a mutex and can be written/read by a couple of set/get functions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Alessandra Ambrogiani alessandr.ambrogian2@studio.unibo.it
 *  	Andrea Belano (dtnperf header, file expiration timer thread) andrea.berlano@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include "Server.h"

#include "../../dtnperf_system_libraries.h"
#include "FileModeHandler/FileModeHandler.h"
#include "ServerAux/ServerAux.h"




//Global variables
static al_types_bundle_object rcvBundle, bundleACK; //necessary for ctrl+c handler

//functions prototypes
dtnperf_error_t serverParsing(application_options_t unparsed_options, int * memoryflag);
void printServerHelp(boolean_t al_bundle_options_help);
void closing_server_handler(int signum);

//Functions

/******************************************************************************
 *
 * \par Function Name:
 *      runServer
 *
 * \brief  Main function of the Server
 *
 * * \par Date Written:
 *  	15/02/20
 *
 * \return dtnperf_error_t					The error code
 *
 * \param[in]	unparsed_options			Options not recognized by the al_bp parser
 * \param[in]	bundle_options				Options already recognized by the al_bp parser
 * \param[in]	registration_descriptor		Identifier of the registration
 *
 *\par Revision History:
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | -----------------| -----------------------------------------------
 *  xx/xx/13 | M. Rodolfi   	| V3 Implementation.
 *  30/11/21 | A. Ambrogiani  	| Initial Implementation of V4
 *  11/07/22 | A.Belano			| Revised version: redesign of file expiration timer thread; use of the new dtnperf header
 *  							| redesign of ctrl+c handler
 *  11/07/22 | C.Caini			| main thread included in runServer,
 *  							| al_bundle_createfunctions moved out of while instructions
 *****************************************************************************/

dtnperf_error_t runServer(bundle_options_t bundle_options, dtn_suite_options_t dtn_suite_options, application_options_t unparsed_options) {

	al_socket_registration_descriptor registration_descriptor;
  	al_error alError;
	al_types_bundle_payload_location pl_location;

	int memoryflag = 0; //default value: saving on file
	dtnperf_error_t error;

    signal(SIGINT, closing_server_handler);

	//parsing
	error=serverParsing(unparsed_options, &memoryflag);
	if (error) return (ERROR); //exit
	alError=al_socket_register(&registration_descriptor, SERVER_DEMUX,SERVER_SERVICE_NUMBER, dtn_suite_options.eid_format_forced, dtn_suite_options.ipn_local, dtn_suite_options.ip, dtn_suite_options.port);
	if (alError) return (ERROR); //exit

	if (memoryflag) {
		pl_location = BP_PAYLOAD_MEM;
	} else {
		pl_location = BP_PAYLOAD_FILE;
	}

		al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Allocate memory for received bundle and bundle ACK... ");

		alError = al_bundle_create(&rcvBundle);
		if (alError != AL_SUCCESS) {
					fflush(stdout);
					al_utilities_debug_print_error("[DTNperf fatal error] in allocating memory for received bundle");
					return (ERROR); //exit
		}

		alError = al_bundle_create(&bundleACK);
		if (alError != AL_SUCCESS) {
			fflush(stdout);
			al_utilities_debug_print_error("[DTNperf fatal error] in allocating memory for ACK bundle");
			al_bundle_free (&rcvBundle);
			return (ERROR); //exit
		}

		al_bundle_options_set(&bundleACK, bundle_options);//Maybe useless.
		al_utilities_debug_print(DEBUG_L2, "Done\n");

		startingFileExpirationTimerThread();

		while (1) {

		al_utilities_debug_print(DEBUG_L1, "[DEBUG_L1] Waiting for the next received bundle...\n");

		al_error error = al_socket_receive(registration_descriptor, &rcvBundle, pl_location, BP_INFINITE_WAIT);

		switch (error) {
		case AL_SUCCESS:
			al_utilities_debug_print(DEBUG_L2, "bundle received from %s. Processing information\n", rcvBundle.spec->source.uri);
			        processingRcvBundle(rcvBundle, bundleACK, registration_descriptor, bundle_options);
			break;
		case AL_WARNING_RECEPINTER:
			al_utilities_debug_print(DEBUG_L2, "Warning in al_socket_receive: Reception is interrupted \n");
			break;
		case AL_WARNING_TIMEOUT://It cannot happen if al_socket_receive is blocking for ever (last parameter is -1)
			al_utilities_debug_print(DEBUG_L2, "Warning in al_socket_receive: Timeout \n");
			break;
		default:
			fflush(stdout);
			al_utilities_debug_print_error("[DTNperf errors] in al_socket_receiver (server) \n");
			int status=1;
			serverCleanExit(status, rcvBundle, bundleACK, registration_descriptor); //rcvBundle e bundlACK are passed in copy but contain only 2 pointers
            return (ERROR); //exit
		}
	} //while()
	return SUCCESS;
}

/******************************************************************************
 *
 * \par Function Name:
 *      serverParsing
 *
 * \brief 	It parses the Server options (from the options not recognized by the
 * 			Unified API bundle parser)
 *
 * * \par Date Written:
 *  	15/02/20
 *
 * \return dtnperf_error_t					The error code
 *
 * \param[in]	unparsed_options			Options not recognized by the al_bp parser
 * \param[out]	memmoryflag					The only option that can be parsed by the Server
 *
 *
 *  DD/MM/YY |  AUTHOR         	|   DESCRIPTION
 *  -------- | -----------------| -----------------------------------------------
 *  30/11/21 | A. Ambrogiani  	| Initial Implementation
 *  11/07/22 | C. Caini			| Revised
 *****************************************************************************/

dtnperf_error_t serverParsing(application_options_t unparsed_options, int * memoryflag) {
//memoryFlag is the only output parameters; debug level is managed apart.
	int optionIndex = 0;
	//long options for getopt_long parsing
#define SKIP 1
#define HELP 2

	 static struct option long_options[] =
		{
	        {"server", no_argument, NULL, SKIP },
			{ "help", no_argument, NULL, HELP },
			{"memory", no_argument, NULL, 'M'},
			{NULL, 0, NULL, 0}	// The last element of the array has to be filled with zeros.
		};

	optind = 1;
	while ((optionIndex = getopt_long(unparsed_options.app_opts_size,
			unparsed_options.app_opts, "M", long_options, NULL)) != -1) {
		switch (optionIndex) {

		case SKIP:
			//skip --server
			break;
		case 'M':
			//-M, --memory
			*memoryflag = 1;
			break;
		case HELP:
			// --help
			printServerHelp(TRUE);
			return ERROR;
			break;
		case '?':
		     printf("Dtnperf server parser error, option not recognized: %s\n", unparsed_options.app_opts[optind-1]);
		     printServerHelp(TRUE);
		     return ERROR;
		     break;
		default:
			break;
		} //switch
	} //while
return SUCCESS;
}

void printServerHelp(boolean_t al_bundle_options_help) {
	printf("\nSyntax:\n");
	printf("--server [all options]\n\n");
	printf("dtnperf server options:\n");
	printf("-M, --memory         	    Save received bundles into memory.\n");
	printf("-h, --help                  This help\n");
	if(al_bundle_options_help == TRUE){
		printf("%s\n", al_bundle_options_get_dtnsuite_help());
	}
}

void serverCleanExit(int status, al_types_bundle_object rcvBundle, al_types_bundle_object bundleACK, al_socket_registration_descriptor registration_descriptor)
	{
		al_bundle_free(&rcvBundle);
		al_bundle_free(&bundleACK);

		al_socket_unregister(registration_descriptor);
		al_socket_destroy();
		al_utilities_debug_print_debugger_destroy();
		printf("Dtnperf Server: exit.\n");
		exit(status);
	return;
}

void closing_server_handler(int signal) {
	al_utilities_debug_print(DEBUG_L1,
			"[DEBUG_L1] A CTRL+c has been intercepted. Proceeding on closing the Server\n");
	set_closing_server (TRUE);
	int status=1;
	serverCleanExit(status, rcvBundle, bundleACK, 1);//the latest 1 is a temporary fix; it should be the reg.descriptor
	return;
}
