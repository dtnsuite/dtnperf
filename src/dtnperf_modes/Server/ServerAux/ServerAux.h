/* \file ServerAux.h
 *
 *  \brief  This file contains the prototype of the function to process the received bundle and few others
 *
 *  \details This file contains the function that processes the received bundle, and the function
 *  			dedicated to send the ACK back to the client, plus a couple of set/get functions to
 *  			get/set the "forced_close" variable used to terminate the file expiration timer thread
 *
 *
 ** \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Alessandra Ambrogiani, alessandr.ambrogian2@studio.unibo.it
 *  \authors Andrea Belano, andrea.belano@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 */


#ifndef SERVER_AUX_H
#define SERVER_AUX_H

#include "../Server.h"

void processingRcvBundle(al_types_bundle_object rcvBundle, al_types_bundle_object bundleACK,
		al_socket_registration_descriptor registrationDescriptor,
		bundle_options_t bundleOptions);

void serverCleanExit();

void set_closing_server(boolean_t value);

boolean_t get_closing_server();


#endif /* SERVER_H*/
