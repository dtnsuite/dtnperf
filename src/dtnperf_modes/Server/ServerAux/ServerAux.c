/** \file ServerAux.c
 *
 *  \brief  This file contains the functions to process the received bundle and to send an ACK
 *
 *  \details This file contains the function that processes the received bundle, and the function
 *  			dedicated to send the ACK back to the client, plus a couple of set/get functions to
 *  			get/set the "forced_close" variable used to terminate the file expiration timer thread
 *
 *
 ** \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  \authors Alessandra Ambrogiani, alessandr.ambrogian2@studio.unibo.it
 *  \authors Andrea Belano, andrea.belano@studio.unibo.it
 *
 *  \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 */

#include "ServerAux.h"

#include "../../../dtnperf_definitions.h"
#include "../FileModeHandler/FileModeHandler.h"


#include <unified_api.h>
#include "../../../dtnperf_system_libraries.h"

//Global variables
static pthread_mutex_t mutex_forced_close;
static boolean_t forced_close = FALSE;

//functions prototypes
void sendBundleAck (al_socket_registration_descriptor registrationDescriptor,
		al_types_bundle_object rcvBundle, dtnperfHeader_t clientHeader, al_types_bundle_object bundleACK);
void print_ext_or_metadata_blocks(uint32_t blocks_len, al_types_extension_block *blocks_val);
//functions


/******************************************************************************
 * \par Function Name:
 *      processingRcvBundle
 *
 * \brief Function to process the bundle received by the Server
 *
 * \param[in] bundleACK, rcvBundle			The bundle ACK and the received bundle structures (to be reused)
 * \param[in] registrationDescriptor		The RegDes used
 * \param[in] bundleOptions		T			The bundle options

 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------------
 *  30/11/21 | A. Ambrogiani 	   | Initial Implementation
 *  20/04/22 | A. Belano, C./Caini |   Revised implementation
 *****************************************************************************/

void processingRcvBundle(al_types_bundle_object rcvBundle, al_types_bundle_object bundleACK,
		al_socket_registration_descriptor registrationDescriptor,
		bundle_options_t bundleOptions) {

	uint32_t bundle_payload_len;
	time_t localtimestamp;
	static int numBundleReceived=0;

	//header_t rcvBundleHeader; TODO:verificare che si possa togliere la malloc
	dtnperfHeader_t* rcvBundleHeader = (dtnperfHeader_t *) malloc(sizeof(dtnperfHeader_t));


	al_error error = al_bundle_get_payload_size(rcvBundle,
			(uint32_t*) &bundle_payload_len);

	if (error != AL_SUCCESS) {
		fflush(stdout);
		al_utilities_debug_print_error(
				"[DTNperf fatal error] in getting bundle payload size \n");
		//serverCleanExit();
	}

	if (bundle_payload_len < MIN_DTNPERF_HEADER_SIZE) {	//Il payload è troppo corto per contenere l'header
		fflush(stdout);
		printf("Header is too short\n");	//TODO: scrivere il messaggio di errore e uscire
		//serverCleanExit();
	}

	al_utilities_debug_print(DEBUG_L2, "Done\n");

	localtimestamp = time(NULL);
    numBundleReceived++;
	printf("%s bundle %d: %u bytes from %s \n", ctime(&localtimestamp), numBundleReceived,
				bundle_payload_len, rcvBundle.spec->source.uri);
	//***********************************************************************//

	if(rcvBundle.spec->extensions.extension_number > 0)
		print_ext_or_metadata_blocks(rcvBundle.spec->extensions.extension_number, rcvBundle.spec->extensions.extension_blocks);

	//***********************************************************************//


	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Getting DTNperf client header...\n");

	error = getDtnperfHeader(rcvBundle, rcvBundleHeader, bundle_payload_len);

	if (error != AL_SUCCESS) {
		printf("Discarding bundle...\n");
		free(rcvBundleHeader);
		return;
	}

	if (rcvBundleHeader->type <= 1) { //if the bundle type is data/time or file  check if the ACK is requested

		if (rcvBundleHeader->ack_QoS.flag == 1) {
			al_utilities_debug_print(DEBUG_L1, "[DEBUG_L1] Sending back Bundle Ack...\n");
			sendBundleAck(registrationDescriptor, rcvBundle, *rcvBundleHeader, bundleACK);
		}
	}


	if (rcvBundleHeader->type == 1) {	//File
		bundleFileHandler(rcvBundle, *rcvBundleHeader, bundle_payload_len);
	}

	free(rcvBundleHeader);
}

/******************************************************************************
 * \par Function Name:
 *      sendBundleACK
 *
 * \brief Function to send the dtnperf client a bundle ACK (when requested by the client)
 *
 * \param[in] registrationDescriptor		The RegDes used
 * \param[in] rcvBundle						The bundle received (bundleX to be ACKed)
 * \param[in] clientHeader					The dtnperf header of the received bundle (bundleX to be ACKed)
 * \param[in] bundleACK						The bundle ACK structure (to be reused)
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------------
 *  20/04/22 | A. Belano, C./Caini |   First implementation
 *****************************************************************************/


void sendBundleAck (al_socket_registration_descriptor registrationDescriptor,
		al_types_bundle_object rcvBundle, dtnperfHeader_t clientHeader, al_types_bundle_object bundleACK){

	dtnperfHeader_t ACKHeader;
	al_error error;
	void* buffer;
    int bufferLength;

//The bundleACK structure is passed in input with default options; set the options that vary from ACK to ACK
    al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Set bundle ACK variable options (lifetime, priority, BP version)...\n");
    al_bundle_set_lifetime(&bundleACK, clientHeader.ack_lifetime);
    al_bundle_set_cardinal_priority(&bundleACK, clientHeader.ack_QoS.priority);
    al_bundle_set_bp_version(&bundleACK,rcvBundle.spec->bp_version );

//Create the ACK header structure reporting bundleX triple (source EID, timestamp, seq)
    al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Set the dtnperf ACK header structure...\n");
    memset(&ACKHeader, 0, sizeof(ACKHeader));
    ACKHeader.version = DTNPERF_HEADER_VERSION;
    ACKHeader.type = 2;	//ACK
    ACKHeader.bundlex_src_eid_length = (uint8_t) strlen(rcvBundle.spec->source.uri);
    char* sourceEID = malloc(ACKHeader.bundlex_src_eid_length + 1);
    strcpy(sourceEID, rcvBundle.spec->source.uri);
    ACKHeader.bundlex_src_eid = sourceEID;
    ACKHeader.bundlex_timestamp= rcvBundle.spec->creation_ts; //both time and seqno

//Serialize the ACK header structure
	al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Serialize the ACK header...\n");
	buffer=malloc(MIN_DTNPERF_HEADER_SIZE+ACKHeader.bundlex_src_eid_length);
    bufferLength=serializeDtnperfHeader(ACKHeader, buffer);//malloc of buffer internal to serialize

//Set the bundle ACK payload and send the bundle
    al_bundle_set_payload_mem(&bundleACK, buffer, bufferLength);
    error = al_socket_send(registrationDescriptor, bundleACK,
    			rcvBundle.spec->source, rcvBundle.spec->report_to);
	switch (error) {
	case AL_SUCCESS:
		al_utilities_debug_print(DEBUG_L1, "[DEBUG_L1] Bundle ACK sent. Destination: %s, timestamp: %llu.%llu\n", ACKHeader.bundlex_src_eid, ACKHeader.bundlex_timestamp.time, ACKHeader.bundlex_timestamp.seqno);
		break;
	default:
	  	al_utilities_debug_print_error("[DTNperf Error] error in sending bundle ACK\n");
		fflush(stdout);
		serverCleanExit(1);
		break;
	} //switch
	free(buffer);
    bundleACK.payload->buf.buf_val = NULL;
    bundleACK.payload->buf.buf_len = 0;
	free (sourceEID);
}


boolean_t get_closing_server()
{
	int result;
	pthread_mutex_lock(&mutex_forced_close);
	result = forced_close;
	pthread_mutex_unlock(&mutex_forced_close);
	return result;
}

void set_closing_server(boolean_t value)
{
	pthread_mutex_lock(&mutex_forced_close);
	forced_close = value;
	pthread_mutex_unlock(&mutex_forced_close);
}


/* Author: Laura Mazzuca, laura.mazzuca@studio.unibo.it*/
void print_ext_or_metadata_blocks(uint32_t blocks_len, al_types_extension_block *blocks_val)
{
	int i=0;
	al_utilities_debug_print(DEBUG_L1,"[DTNperf L1] number of blocks: %lu\n", blocks_len);
	for (i = 0; i < blocks_len; i++)
	{
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1] Block[%d]\n", i);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---type: %lu\n", blocks_val[i].block_type_code);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---flags: %lu\n", blocks_val[i].block_processing_control_flags);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---data_len: %lu\n", blocks_val[i].block_data.block_type_specific_data_len);
		al_utilities_debug_print(DEBUG_L1,"[DEBUG_L1]---data_val: %s\n", blocks_val[i].block_data.block_type_specific_data);
	}
	return;
}

