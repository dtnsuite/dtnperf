var searchData=
[
  ['buffer_3',['buffer',['../_sender_a_c_k_8c.html#aff2566f4c366b48d73479bef43ee4d2e',1,'SenderACK.c']]],
  ['buffer_5fdim_4',['BUFFER_DIM',['../_rcv_bundle_handler_8c.html#ab6b9a77ffbec77e74f2839d4add6880f',1,'RcvBundleHandler.c']]],
  ['buffer_5ffrag_5',['BUFFER_FRAG',['../_rcv_bundle_handler_8c.html#ab0cb7a6312dd7177dd2f7ab298919614',1,'RcvBundleHandler.c']]],
  ['bundle_5foptions_6',['bundle_options',['../structparams.html#acade25c78fcc68c439ae0398708a4324',1,'params']]],
  ['bundleack_7',['BundleACK',['../struct_bundle_a_c_k.html',1,'']]],
  ['bundlefilehandler_8',['bundleFileHandler',['../_file_mode_handler_8c.html#a20cc8380f602ae7c19c0b502abdd4725',1,'bundleFileHandler(al_bp_bundle_object_t bundle_object, ClientHeader clientHeader, char *directory, char *fragment, int fragmentLen):&#160;FileModeHandler.c'],['../_file_mode_handler_8h.html#a20cc8380f602ae7c19c0b502abdd4725',1,'bundleFileHandler(al_bp_bundle_object_t bundle_object, ClientHeader clientHeader, char *directory, char *fragment, int fragmentLen):&#160;FileModeHandler.c']]]
];
