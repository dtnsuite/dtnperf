var indexSectionsWithContent =
{
  0: "_abcdefgimprst",
  1: "bp",
  2: "dfrs",
  3: "_abcgprs",
  4: "bcdefmprst",
  5: "p",
  6: "bdfi",
  7: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Pages"
};

