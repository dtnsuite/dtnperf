/** \file FileModeHandler.h
 *
 *  \brief  This file contains the prototypes of the functions in FileModeHandler.c
 *
 *  \details Bundle sent by the Client in File mode contain file segments in the DTNperf payload; these segments must be reassembled by the Server.
 *  To this end, segments are saved in temporary hidden files in the dtnperf_files directory (under user's home), until file completion.
 *  This file contains the the functions to assemble the segments into a complete file. If the assembling is not complete after a time threshold
 *  determined by a field contained in dthe DTNperf headers of data bundle received, the incomplete files are canceled.
 *  Incomplete files are periodically checked by the file expiration timer thread, whose functions are also contained in this file.
 *
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **
 * \authors of present version
 *  	Andrea Belano andrea.berlano@studio.unibo.it
 *
 * \par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/



#ifndef FILEMODEHANDLER_H
#define FILEMODEHANDLER_H

#include "../../../dtnperfHeaders.h"

typedef struct list {

	struct list* next;
	uint32_t element;

} list_t;

typedef struct pending_file {

	char* path;
	uint32_t total_length;
	list_t* fragments;
	uint32_t current_length;
	uint64_t expiration;

	struct pending_file* next;

} pending_file_t;

typedef struct {

	int elements;
	pending_file_t* head;
	pending_file_t* last;
	pending_file_t* current;

} pending_files_list;

void bundleFileHandler(al_types_bundle_object bundle_object, dtnperfHeader_t clientHeader, uint32_t payload_length);
void startingFileExpirationTimerThread();

#endif /*FILEMODEHANDLER_H */
