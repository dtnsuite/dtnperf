/** \file FileModeHandler.c
 *
 *  \brief  This file contains the functions used by the Server to handle data bundles sent in File mode by the Client
 *
 *  \details Bundle sent by the Client in File mode contain file segments in the DTNperf payload; these segments must be reassembled by the Server.
 *  To this end, segments are saved in temporary hidden files (called pendingFiles) in the dtnperf_files directory (under user's home), until file completion.
 *  The FileMopdeHandler.c file contains the the functions to assemble the segments into a complete file. If the assembling is not complete after a time threshold
 *  determined by a field contained in the DTNperf headers of data bundle received, the incomplete files are canceled.
 *  Incomplete files are periodically checked by the file expiration timer thread, whose functions are also contained in this file.
 *  To implement the policy above, a list is initialized at start up. Each element consist of a state structure ("pending_file_t" type) referring to one
 *  pending file. The list is checked to cope with duplicated segments, to verify file completion and last but not least, to eliminate
 *  pending files that have not been completed in the maximum allowed time.
 *  To make the state of pending files persistent between consecutive sessions of the Server, each element of the list is also saved into an hidden state file.
 *  At Server start up, when the list is initialized, the state files are read and loaded into the list, after verifying their integrity by checking their CRC
 *  This may be useful in case of closing anomalies, such as power disruptions.
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **
 * \authors of present version
 *  	Andrea Belano andrea.berlano@studio.unibo.it
 *
 * \par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/

#include <stdbool.h>

#include "FileModeHandler.h"

#include "../../../dtnperf_system_libraries.h"
#include "../../../utils.h"//CCaini because it uses CRC

#include "../../../dtnperf_definitions.h"
#include "../ServerAux/ServerAux.h"


//Global variables

static pthread_t fileExpTimer;
static sem_t list_sem;

static pending_files_list pendingFiles;
static bool ready;	//Signals the file is complete and can be renamed

//Prototypes
void removeStateFile(char* path);
void createFile(pending_file_t* pending_file);

void initList();
bool insertInList(char* path, uint32_t total_length, uint32_t frag_offset, uint64_t expiration, uint32_t frag_size);
void removeFromList(char* path);


//Functions

/******************************************************************************
 * \par Function Name:
 *      _fileExpirationTimer
 *
 * \brief Main function of the file expiration timer thread
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   First imlemenmtation
 *****************************************************************************/


void* _fileExpirationTimer(void *opt) {

	struct timeval tv;
	uint64_t localtimestamp;

	pthread_detach(fileExpTimer);
	while (!get_closing_server()) {

		al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] checking for expired files...\n");


		sem_wait(&list_sem);

		gettimeofday(&tv, NULL);
		localtimestamp = tv.tv_sec * 1000 + tv.tv_usec / 1000 - 946684800000;

		if (pendingFiles.elements != 0) {
			for (int i = 0; (i < FILES_TO_CHECK_PER_CYCLE || !FILES_TO_CHECK_PER_CYCLE) && pendingFiles.current != NULL; i++) {

				if (localtimestamp > pendingFiles.current->expiration) {
					printf("[WARNING]: the file %s has expired, deleting file\n", pendingFiles.current->path);
					remove(pendingFiles.current->path);
					removeFromList(pendingFiles.current->path);
				} else {
					pendingFiles.current = pendingFiles.current->next;
				}

			}

			if (pendingFiles.current == NULL) {
				pendingFiles.current = pendingFiles.head;
			}
		}

		sem_post(&list_sem);

		sleep(FILE_CHECK_PERIOD);
	}

	sem_destroy(&list_sem);
	pthread_exit(0);
}

/******************************************************************************
 * \par Function Name:
 *      startingFileExpirationTimerThread
 *
 * \brief Function to initialize the list of pending files and start the timer thread
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   First implementation
 *****************************************************************************/

void startingFileExpirationTimerThread() {
	initList();
	ready = 0;

	sem_init(&list_sem, 0, 1);
	pthread_create(&fileExpTimer, NULL, _fileExpirationTimer, NULL);
}

/******************************************************************************
 * \par Function Name:
 *      createFile
 *
 * \brief Function to serialize an element of the list into an hidden state file
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   First implementation
 *****************************************************************************/

void createFile(pending_file_t* pending_file) {

	int fd;

	char filename[FILENAME_MAX];
	uint32_t id = 0;

	char buf[8196];
	uint32_t crc = 0;
	int offset = 0;
	uint16_t pathlen = strlen(pending_file->path);
	list_t* cur_frag;

	id = calc_crc32_d8(id, (uint8_t *) pending_file->path, strlen(pending_file->path));
	sprintf(filename, "%s%s/.%u", getenv("HOME"), FILE_DIR, id);

	memcpy(buf + offset, &pathlen, 2);
	offset += 2;

	memcpy(buf + offset, pending_file->path, pathlen);
	offset += pathlen;

	memcpy(buf + offset, &(pending_file->total_length), sizeof(pending_file->total_length));
	offset += sizeof(pending_file->total_length);

	memcpy(buf + offset, &(pending_file->current_length), sizeof(pending_file->current_length));
	offset += sizeof(pending_file->current_length);

	memcpy(buf + offset, &(pending_file->expiration), sizeof(pending_file->expiration));
	offset += sizeof(pending_file->expiration);

	cur_frag = pending_file->fragments;
	while (cur_frag != NULL) {
		memcpy(buf + offset, &(cur_frag->element), sizeof(cur_frag->element));
		offset += sizeof(cur_frag->element);
		cur_frag = cur_frag->next;
	}

	fd = open(filename, O_CREAT | O_WRONLY | O_TRUNC | O_NONBLOCK, 0755);

	if (fd < 0) {
		printf("[WARNING] could not open file %s, the file %s might be lost between sessions\n", filename, pending_file->path);
	} else {
		if (write(fd, buf, offset) < 0) {
			printf("[WARNING] error in writing file %s, the file %s might be lost between sessions\n", filename, pending_file->path);
		}

		crc = calc_crc32_d8(crc, (uint8_t *) buf, offset);
		if (write(fd, &crc, sizeof(crc)) < 0) {
			printf("[WARNING] error in writing file %s, the file %s might be lost between sessions\n", filename, pending_file->path);
		}

		close(fd);
	}

	al_utilities_debug_print(DEBUG_L2, "Saved file %s\n", filename);

}

/******************************************************************************
 * \par Function Name:
 *      removeStateFile
 *
 * \brief Function to remove an hidden state file from the file system
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   First implementation
 *****************************************************************************/

void removeStateFile(char* path) {

	char filename[FILENAME_MAX];
	uint32_t id = 0;

	id = calc_crc32_d8(id, (uint8_t *) path, strlen(path));
	sprintf(filename, "%s%s/.%u", getenv("HOME"), FILE_DIR, id);

	remove(filename);

}

/******************************************************************************
 * \par Function Name:
 *      initList
 *
 * \brief Function to initialize the list of pending files
 *
 * \details At start up, the Server initialize the list of pending files by reading
 *  the hidden  state files. To ensure consistency, the file integrity is verified by
 *  means of a CRC check.
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   Revision
 *****************************************************************************/

void initList() {
	memset(&pendingFiles, 0, sizeof(pending_files_list));

	char dirname[FILENAME_MAX];
	char filename[FILENAME_MAX];

	DIR* dir;
	struct dirent* element;
	int fd, nRead;
	char buf[8196];
	int offset = 0;
	uint32_t crc;
	uint32_t crc_c;

	uint16_t pathlen;
	char* path;
	list_t* current_fragment;

	sprintf(dirname, "%s%s/", getenv("HOME"), FILE_DIR);

	if (mkdir(dirname, 0777) < 0) {
		if (errno == EEXIST) {
			al_utilities_debug_print(DEBUG_L2,
							"[DEBUG_L2] FileMode: Directory %s already exists\n", dirname);
		} else {
			al_utilities_debug_print(DEBUG_L2,
							"[DEBUG_L2] FileMode: Fail in creating the directory %s\n", dirname);
			perror("mkdir");
			return;
		}
	} else {
		al_utilities_debug_print(DEBUG_L2,
						"[DEBUG_L2] FileMode: Directory doesn't exist. Creating directory %s\n", dirname);
	}

	dir = opendir(dirname);


	if (dir == NULL) {
		printf("[WARNING] could not open directory %s: possible file loss\n", dirname);
		return;
	}

	while ((element = readdir(dir)) != NULL) {
		if (element->d_type == DT_REG) {

			strcpy(filename, dirname);
			strcat(filename, element->d_name);

			fd = open(filename, O_RDONLY);
			if (fd  < 0) {
				printf("[WARNING] could not open file %s\n", filename);
				continue;
			}

			nRead = read(fd, buf, 8196);
			if (nRead  < 0) {
				printf("[WARNING] could not read file %s\n", filename);
				continue;
			}

			crc_c = 0;
			crc = *((uint32_t *) (buf + nRead - 4));
			crc_c = calc_crc32_d8(crc_c, (uint8_t *) buf, nRead - 4);

			if (crc_c != crc) {
				printf("[WARNING] the file %s is corrupted; removing file\n", filename);
				remove(filename);
				continue;
			}

			if (pendingFiles.elements != 0) {

				pendingFiles.last->next = (pending_file_t *) malloc(sizeof(pending_file_t));
				memset(pendingFiles.last->next, 0, sizeof(pending_file_t));
				pendingFiles.last = pendingFiles.last->next;

			} else {

				pendingFiles.last = (pending_file_t *) malloc(sizeof(pending_file_t));
				memset(pendingFiles.last, 0, sizeof(pending_file_t));
				pendingFiles.current = pendingFiles.last;
				pendingFiles.head = pendingFiles.last;

			}

			pendingFiles.elements++;

			pathlen = *((uint16_t *) buf);
			offset += 2;
			path = malloc(pathlen + 1);
			memcpy(path, buf + offset, pathlen);
			offset += pathlen;
			path[pathlen] = '\0';
			pendingFiles.last->path = path;

			pendingFiles.last->total_length = *((uint32_t *) (buf + offset));
			offset += 4;

			pendingFiles.last->current_length = *((uint32_t *) (buf + offset));
			offset += 4;

			pendingFiles.last->expiration = *((uint64_t *) (buf + offset));
			offset += 8;

			pendingFiles.last->fragments = (list_t *) malloc(sizeof(list_t));	//Loading the first fragment
			pendingFiles.last->fragments->element = *((uint32_t *) (buf + offset));
			offset += 4;

			current_fragment = pendingFiles.last->fragments;

			for (; offset < nRead - 4; offset += 4) {		//The others (if any)
				current_fragment->next = (list_t *) malloc(sizeof(list_t));
				current_fragment = current_fragment->next;
				current_fragment->element = *((uint32_t *) (buf + offset));
			}

			pendingFiles.last->fragments->next = NULL;

			al_utilities_debug_print(DEBUG_L2, "[DEBUG L2] successfully loaded the data structure referring to the file %s\n", path);

		}
	}

	pendingFiles.current = pendingFiles.head;
	closedir(dir);
}

/******************************************************************************
 * \par Function Name:
 *      insertInList
 *
 * \brief At the reception of a new segment, this function either updates the correspondent element of the list,
 * if the pending file already exists (this segment is not the first), or it insert a new element in the list
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   Revision
 *****************************************************************************/


bool insertInList(char* path, uint32_t total_length, uint32_t frag_offset, uint64_t expiration, uint32_t frag_size) {

	pending_file_t* cur = pendingFiles.head;
	list_t* fragments;
	bool res = 1;
	bool isPresent = 0;

	while (cur != NULL) {
		if (!strcmp(cur->path, path)) {

			isPresent = 1;
			fragments = cur->fragments;

			while (fragments->next != NULL) {
				if (fragments->element == frag_offset) {
					al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Warning: duplicated bundle\n");
					res = 0;
					break;
				}
				fragments = fragments->next;
			}

			if (res && fragments->element == frag_offset) {	//Checking the last element
				al_utilities_debug_print(DEBUG_L2, "[DEBUG_L2] Warning: duplicated bundle\n");
				res = 0;
			}

			if (res) {	//The fragment is NOT duplicated
				fragments->next = (list_t*) malloc(sizeof(list_t));
				memset(fragments->next, 0, sizeof(list_t));
				fragments->next->element = frag_offset;

				cur->current_length += frag_size;
			}

			if (res && cur->current_length == cur->total_length) {	//The file is complete
				removeFromList(path);
				ready = 1;
			} else {
				createFile(cur);
			}

			break;
		}
	}

	if (!isPresent) {	//The File indicated in the DTNperf header of the received bundle is not in the list of pending files
//In other words, the segment arrived is the first of a new File

		if (pendingFiles.elements != 0) { //There are other pending files

			pendingFiles.last->next = (pending_file_t *) malloc(sizeof(pending_file_t));
			memset(pendingFiles.last->next, 0, sizeof(pending_file_t));
			pendingFiles.last = pendingFiles.last->next;

		} else {	//The list is empty (There are no pending files)

			pendingFiles.last = (pending_file_t *) malloc(sizeof(pending_file_t));
			memset(pendingFiles.last, 0, sizeof(pending_file_t));
			pendingFiles.head = pendingFiles.last;
			pendingFiles.current = pendingFiles.last;

		}

		pendingFiles.elements++; //increase the counter of list elements
//write all fields
		pendingFiles.last->total_length = total_length;
		pendingFiles.last->expiration = expiration;

		pendingFiles.last->fragments = (list_t *) malloc(sizeof(list_t));
		memset(pendingFiles.last->fragments, 0, sizeof(list_t));
		pendingFiles.last->fragments->element = frag_offset;
		pendingFiles.last->current_length = frag_size;

		pendingFiles.last->path = (char *) malloc(strlen(path) + 1);
		strcpy(pendingFiles.last->path, path);
//CCaini Most likely the control could be anticipated to avoid inserting/deleting an element in the list
		if (pendingFiles.last->current_length == pendingFiles.last->total_length) {	//The file is complete
			removeFromList(path);
			ready = 1;
		} else {
			createFile(pendingFiles.last);
		}

	}

	return res;
}

/******************************************************************************
 * \par Function Name:
 *      removeFromList
 *
 * \brief When a pending file is completed, this function removes the hidden state file
 * and the corresponding list element
 * the list
 *
 * \par Date Written:
 *      25/04/22
 * \par Revision History:
 *
 *  DD/MM/YY |  AUTHOR             |   DESCRIPTION
 *  -------- | ------------------- | -----------------------------------------
 *  25/04/22 | A. Belano		   |   Revision
 *****************************************************************************/

void removeFromList(char* path) {

	removeStateFile(path); //Remove the hidden state file

	//Remove the corresponding list element
	if (pendingFiles.head == NULL) {
		return;
	}

	pending_file_t* prev = pendingFiles.head;
	pending_file_t* cur = prev->next;

	list_t* frag_prev;
	list_t* frag_cur;

	while (cur != NULL) {
		if (!strcmp(cur->path, path)) {

			frag_prev = cur->fragments;
			frag_cur = frag_prev->next;

			while (frag_cur != NULL) {
				free(frag_prev);
				frag_prev = frag_cur;
				frag_cur = frag_cur->next;
			}
			free(frag_prev);	//Freeing the last element

			prev->next = cur->next;

			if (!strcmp(cur->path, pendingFiles.head->path)) {
				pendingFiles.head = cur->next;
			}
			if (!strcmp(cur->path, pendingFiles.current->path)) {
				pendingFiles.current = cur->next;
			}
			if (!strcmp(cur->path, pendingFiles.last->path)) {
				pendingFiles.last = NULL;
			}

			free(cur->path);
			free(cur);
			break;
		}

		prev = cur;
		cur = cur->next;
	}

	if (prev != NULL && !strcmp(prev->path, path)) {	//If the list is only 1 element long
		frag_prev = prev->fragments;
		frag_cur = frag_prev->next;

		while (frag_cur != NULL) {
			free(frag_prev);
			frag_prev = frag_cur;
			frag_cur = frag_cur->next;
		}
		free(frag_prev);	//Freeing the last element

		if (!strcmp(prev->path, pendingFiles.head->path)) {
			pendingFiles.head = prev->next;
		}
		if (!strcmp(prev->path, pendingFiles.current->path)) {
			pendingFiles.current = prev->next;
		}
		if (!strcmp(prev->path, pendingFiles.last->path)) {
			pendingFiles.last = NULL;
		}

		free(prev->path);
		free(prev);
	}

	pendingFiles.elements--; //decrease the counter of pending files

}

/******************************************************************************
 *
 * \par Function Name:
 *      bundleFileHandler
 *
 * \brief  Function to extract the file segment from the arrived bundle and to insert it the pending file
 *
 * \return void
 *
 * \param[in]	bundle_object				The al_bp bundle
 * \param[in]	clientHeader			    The DTNperf header, used to retrieve file name, size and fragment offset
 * \param[in]	payload_length	         	The length of the payload
 *
 *
 *****************************************************************************/

void bundleFileHandler(al_types_bundle_object bundle_object,
		dtnperfHeader_t clientHeader,
		uint32_t payload_length) {

	//PATH: FILE_DIR/<node-nbr|node-name>/<original_filename>

//We want one directory for each dtn node. It is thus necessary to extract the node from the source EID
	char dirname[FILENAME_MAX];
	char eid[AL_TYPES_MAX_EID_LENGTH];
	char* node = eid;
	int offset = 1;

	sprintf(dirname, "%s%s/", getenv("HOME"), FILE_DIR);

	strncpy(eid, bundle_object.spec->source.uri, AL_TYPES_MAX_EID_LENGTH);//copy the source eid into eid, which is pointed by node
	//Determine whether the eid scheme is ipn or dtn by checking the first character; We assume the eid is well-formed
	if (*node == 'i') {	//ipn
		node = node + 4;

		while (*(node + offset) != '.') {
			offset++;
		}

	} else {	//dtn
		node = node + 6;

		while (*(node + offset) != '/') {
			offset++;
		}
	}

	*(node + offset) = '\0';

	//Now the variable node actually represents the node number or the node name

	strcat(dirname, node); //concatenate dirname to node

	if (mkdir(dirname, 0777) < 0) {
		if (errno == EEXIST) {
			al_utilities_debug_print(DEBUG_L2,
										"[DEBUG_L2] FileMode: Directory %s already exists\n", dirname);
		} else {
			al_utilities_debug_print(DEBUG_L2,
										"[DEBUG_L2] FileMode: Fail in creating the directory %s\n", dirname);
			perror("mkdir");
			return;
		}
	} else {
		al_utilities_debug_print(DEBUG_L2,
								"[DEBUG_L2] FileMode: Directory doesn't exist. Creating directory %s\n", dirname);
	}

	strcat(dirname, "/.");	//The pending file is hidden until it is complete
	strncat(dirname, clientHeader.file_name, clientHeader.file_name_length);

	struct timeval tv;
	gettimeofday(&tv, NULL);
	uint64_t localtimestamp = tv.tv_sec * 1000 + tv.tv_usec / 1000 - ((uint64_t)DTN_EPOCH)*1000;
//CCaini Warning: it works in bpv7 but not in bpv6
	if (localtimestamp - bundle_object.spec->creation_ts.time > ((uint64_t) clientHeader.file_exp) * 1000) {
		int res = remove(dirname);

		sem_wait(&list_sem);
		removeFromList(dirname);
		sem_post(&list_sem);

		if (res == 0) {
			al_utilities_debug_print(DEBUG_L2,
					"[DEBUG_L2] Expiration file. The file was deleted\n");
		}
	} else {

		sem_wait(&list_sem);
		bool insertionRes = insertInList(dirname, clientHeader.file_size, clientHeader.segment_offset, ((uint64_t) clientHeader.file_exp) * 1000 + bundle_object.spec->creation_ts.time, getPayloadLength(bundle_object) - MIN_DTNPERF_HEADER_SIZE - clientHeader.file_name_length);
		sem_post(&list_sem);

		if (insertionRes) {
			int fd = open(dirname, O_RDWR | O_CREAT, 0755);

			if (fd < 0) {
				perror("open");
				return;
			}

			int res = ftruncate(fd, clientHeader.file_size);	//Setting the file dimension

			if (res < 0) {
				perror("ftruncate");
			} else {

				int payload_offset = MIN_DTNPERF_HEADER_SIZE + clientHeader.file_name_length;	//20 is the length of the DTNperf header fixed part
				lseek(fd, clientHeader.segment_offset, SEEK_SET);

				if (bundle_object.payload->location == BP_PAYLOAD_MEM) {
					if (write(fd, bundle_object.payload->buf.buf_val + payload_offset, payload_length - payload_offset) < 0) {
						printf("[WARNING]: could not write on file %s\n", dirname);
					}
				} else {

					char buffer[1024];
					int nRead;
					int bundle_fd = open(bundle_object.payload->filename.filename_val, O_RDONLY);

					if (bundle_fd < 0) {
						printf("[ERROR] in opening file %s\n", bundle_object.payload->filename.filename_val);
					} else {
						lseek(bundle_fd, payload_offset, SEEK_SET);
						while ((nRead = read(bundle_fd, buffer, sizeof(buffer))) > 0) {
							if (write(fd, buffer, nRead) < 0) {
								printf("[WARNING]: could not write on file %s\n", dirname);
								break;
							}
						}
						close(bundle_fd);
					}
				}

			}

			close(fd);

			if (ready) {	//The file is complete

				char newPath[FILENAME_MAX];
				int delimiterOffset = 1;

				strncpy(newPath, dirname, FILENAME_MAX);
				char* ptr = newPath + strlen(newPath) - 1;

				while (*ptr != '.' || *(ptr - 1) != '/') {
					ptr--;
					delimiterOffset++;
				}

				memcpy(ptr, ptr + 1, delimiterOffset);

				rename(dirname, newPath);
				ready = 0;
			}
		}
	}

}
