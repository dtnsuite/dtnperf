/** \file Server.h
 *
 *\brief  This file contains the prototype of functions and structures implemented in server.c that should be seen outside the Server
 *
 ** \copyright Copyright (c) 2013, 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 **
 ** \par License
 **
 **    This file is part of DTNperf.                                            <br>
 **                                                                               <br>
 **    DTNperf is free software: you can redistribute it and/or modify
 **    it under the terms of the GNU General Public License as published by
 **    the Free Software Foundation, either version 3 of the License, or
 **    (at your option) any later version.                                        <br>
 **    DTNperf is distributed in the hope that it will be useful,
 **    but WITHOUT ANY WARRANTY; without even the implied warranty of
 **    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **    GNU General Public License for more details.                               <br>
 **                                                                               <br>
 **    You should have received a copy of the GNU General Public License
 **    along with DTNperf.  If not, see <http://www.gnu.org/licenses/>.
 **
 **  Authors of old versions:
 **		Michele Rodolfi, michele.rodolfi@studio.unibo.it
 **     Anna d'Amico, anna.damico@studio.unibo.it
 **     Davide Pallotti, davide.pallotti@studio.unibo.it
 **     Marco Bertolazzi, marco.bertolazzi3@studio.unibo.it
 **     Andrea Bisacchi, andrea.bisacchi5@studio.unibo.it
 **
 *\authors of present version
 *  	Alessandra Ambrogiani alessandr.ambrogian2@studio.unibo.it
 *  	Andrea Belano (dtnperf header, file expiration timer thread) andrea.berlano@studio.unibo.it
 *      Carlo Caini (DTNperf project supervisor), carlo.caini@unibo.it
 *
 *\par Supervisor
 *      Carlo Caini, carlo.caini@unibo.it
 ********************************************************/


#ifndef SERVER_H
#define SERVER_H


#include <unified_api.h>
#include "../../dtnperf_definitions.h"

dtnperf_error_t runServer(bundle_options_t bundle_options,
		dtn_suite_options_t dtnsuite_options,
		application_options_t unparsed_options);


#endif /* SERVER_H*/

