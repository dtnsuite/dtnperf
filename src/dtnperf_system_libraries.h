/** \file dtnperf_system_libraries.h
 *
 * \brief This file contains a list of external .h files used in DTNperf
 *
 * \copyright Copyright (c) 2022, Alma Mater Studiorum, University of Bologna, All rights reserved.
 *
 * \author Michele Rodolfi, michele.rodolfi@studio.unibo.it
 *
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 *
 ***********************************************/

#ifndef DTNPERF_SYSTEM_LIBRARIES_H_
#define DTNPERF_SYSTEM_LIBRARIES_H_

#define _GNU_SOURCE
#include <features.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/file.h>
#include <time.h>
#include <assert.h>
#include <pthread.h>
#include <getopt.h>
#include <signal.h>
#include <sys/wait.h>
#include <inttypes.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <arpa/inet.h>
#include <math.h>
#include <semaphore.h>
#endif /*DTNPERF_SYSTEM_LIBRARIES_H_*/
