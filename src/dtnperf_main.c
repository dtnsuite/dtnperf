/** \file dtnperf_main.c
 * 
 * \brief This file contains the main method of dtnperf application
 * 
 * \copyright Copyright (c) 2020, Alma Mater Studiorum, University of Bologna, All rights reserved.
 * 
 * \author Antony Zappacosta, antony.zappacosta@studio.unibo.it
 * 
 * \par Supervisor
 *          Carlo Caini, carlo.caini@unibo.it
 * 
 ***********************************************/
#include "dtnperf_modes/Client/Client.h"
#include "dtnperf_modes/Server/Server.h"
#include "dtnperf_modes/Monitor/Monitor.h"
#include "dtnperf_definitions.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <unified_api.h>


int main(int argc, char **argv)  {

	bundle_options_t bundle_options;
	dtn_suite_options_t dtn_suite_options;
	application_options_t residual_options;

	al_error alError;

	printf("DTNperf version: %s\n", DTNPERF_VERSION);



	alError= al_socket_init(); //CCaini
	if(alError) exit(1);


	alError = al_bundle_options_parse(argc, argv, &bundle_options, &dtn_suite_options, &residual_options);
	if(alError){
		exit(1);
	}
	//It must be called after al_bundle_options_parse as it requires the debug level
	al_utilities_debug_print_debugger_init(dtn_suite_options.debug_level, FALSE, "DTNperf_log");

	if (argc > 1 && !strcmp(argv[1], "--client")) {
		runClient(bundle_options, dtn_suite_options, residual_options);
	} else if (argc > 1 && !strcmp(argv[1], "--server")) {
		runServer(bundle_options, dtn_suite_options, residual_options);
//From the Server  you can only exit by entrering ctrl+C, which terminates dtnperf
	} else if (argc > 1 && (!strcmp(argv[1], "--monitor"))) {
		runMonitor(dtn_suite_options, residual_options);
//From the Monitor you can only exit by entrering ctrl+C, which terminates dtnperf
	} else { // print DTNperf help
		printf("\nSYNTAX: dtnperf <operative mode> [options]\n\n");
		printf("operative modes:\n--server\n--client\n--monitor\n\n");
		printf(	"For dtnperf mode options enter\ndtnperf <operative mode> --help\n");
	}
//Normal termination of the client
	al_socket_destroy();
	al_utilities_debug_print_debugger_destroy();

	exit(0);

}
