# DTNperf
DTNperf_4 is the fourth major version of the DTNperf tool, developed at University of Bologna. It is part of the DTNsuite, which consists of several DTN applications that make use of the Unified API (ex Abstraction Layer), plus the Unified API itself.

As suggested by its name, DTNperf is a tool for performance evaluation in Bundle Protocol DTN environments.

Being built on top of the new Unified API (ex Abstraction Layer), DTNperf is compatible with all the most important bundle protocols implementations (DTN2, DTNME, ION, IBR_DTN, uD3TN and Unibo-BP).

## Guide
Please refer to the DTNperf .pdf guide for information on:
- release notes
- building instructions
- a brief overview and a concise user guide.

Further information (DTNperf_3)
C. Caini, A. d’Amico and M. Rodolfi, “DTNperf_3: a Further Enhanced Tool for Delay-/Disruption- Tolerant Networking Performance Evaluation”, in Proc. of IEEE Globecom 2013, 
Atlanta, USA, December 2013, pp. 3009 - 3015. <https://doi.org/10.1109/GLOCOM.2013.6831533>

# Copyright

Copyright (c) 2021, University of Bologna.

# License

DTNperf_4 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, ei>
DTNperf_4 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR>
You should have received a copy of the GNU General Public License along with DTNperf_4. If not, see <http://www.gnu.org/licenses/>.


## Authors
- Carlo Caini (Academic supervisor, carlo.caini@unibo.it)
- Antony Zappacosta (part of the client)
- Alessandra Ambrogiani (part of the server)
- Andrea Belano (new DTNperf headers, part of the server)
- Benedetta Rogato (part of the monitor)
- Silvia Lanzoni (Unified API)
- Andrea Bisacchi (co-supervisor of v4 and of Unified API) 
- Michele Rodolfi (main author of version 3)
